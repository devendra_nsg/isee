<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Copyright:GlobalSoftTree
* Author:Devendra
*/

class Learnresource extends CI_Controller {
    
	
	public function __construct(){		
	   parent::__construct();
			
	   $this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(1));
	   $this->load->model('level_resource_model');
	   $this->load->model('learningresource_model');
	   $this->load->model('block_model');
	   $this->load->model('level_model');
	   $this->load->model('type_model');
	   
	   $this->data['law'] = $this->block_model->get_block(2); 
	}
	
	public function type()
	{   
	     try{
          $this->data['welcome_resource'] = $this->block_model->get_block(7);
		  $this->data['query']['type'] = $this->type_model->get_all_type();
		  
	  }catch (Exception $e){
		  
		 echo $e; die;
	  }	
		$this->load->template('learnresource/type',$this->data);
	}

    public function level()
	{   
        try{
		  $this->data['type'] = $this->learningresource_model->getTypeById($this->uri->segment(4));
		  $this->data['query']['level'] = $this->level_resource_model->get_all_level_resource();
		  
	  }catch (Exception $e){
		  
		 echo $e; die;
	  }	
		$this->load->template('learnresource/level',$this->data);
	}
	
	public function topic()
	{
		$this->data['type'] = $this->learningresource_model->getTypeById($this->uri->segment(4));
		$this->data['level'] = $this->learningresource_model->getLevelById($this->uri->segment(6));
		$this->data['topics'] = $this->learningresource_model->get_learning_resource_topic();
		$this->load->template('learnresource/topic',$this->data);
	}
	
	public function subject()
	{
		$this->data['type'] = $this->learningresource_model->getTypeById($this->uri->segment(4));
		$this->data['level'] = $this->learningresource_model->getLevelById($this->uri->segment(6));
		$this->data['topic'] = $this->learningresource_model->getTopicById($this->uri->segment(8));
		$this->data['subjects'] = $this->learningresource_model->get_learning_resource_subject($this->uri->segment(8));
		$this->load->template('learnresource/subject',$this->data);
	}
	
	public function flashcard()
	{
		$this->data['type'] = $this->uri->segment(4);
		$this->data['level'] = $this->learningresource_model->getLevelById($this->uri->segment(6));
		$this->data['topic'] = $this->uri->segment(8);
		$this->data['subject'] = $this->uri->segment(10);
	
		if($this->data['type']==1) {
			$this->data['question'] = $this->learningresource_model->get_learning_resource_question($this->data['type'], $this->uri->segment(8), $this->data['topic'], $this->data['subject']);
			$this->load->template('learnresource/flashcard', $this->data);
		} elseif($this->data['type']==2) {
			$this->data['videos'] = $this->learningresource_model->get_learning_resource_videos();
			$this->load->template('learnresource/videos', $this->data);
		}
	}

}
