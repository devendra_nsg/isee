<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Index Page for this controller.
* Copyright:GlobalSoftTree
* Author:Devendra
*/

class Fulltest extends CI_Controller {
    
	
	public function __construct(){		
	   parent::__construct();
			
	   $this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(1));
	   
	   $this->load->model('exam_model');
	   $this->load->model('section_model');
	   $this->load->model('level_model');
	   $this->load->model('block_model');
	   $this->data['law'] = $this->block_model->get_block(2); 
	}
	
	
	
	public function index()
	{
	  try{

		  $this->data['query']['level'] = $this->level_model->get_all_level();
		  
	  }catch (Exception $e){
		  
		 echo $e; die;
	  }	
	  $this->load->template('fulltest/level',$this->data);
	}
	
	
	public function welcome()
	{
	  try{

		  $this->data['query']['section'] = $this->section_model->get_all_section();
		  
	  }catch (Exception $e){
		  
		 echo $e; die;
	  }	
	  $this->load->template('fulltest/welcome',$this->data);
	}

     public function exam()
	{
	  try{

		  $this->data['query']['exam'] = $this->exam_model->get_all_exam();
		  //$this->data['law'] = $this->block_model->get_block(2);
		  
	  }catch (Exception $e){
		  
		 echo $e; die;
	  }	
	  $this->load->template('fulltest/exam',$this->data);
	}
	
	public function sample()
	{
	  try{

		  $this->data['query']['exam'] = $this->exam_model->get_all_exam();
		  //$this->data['law'] = $this->block_model->get_block(2);
		  
	  }catch (Exception $e){
		  
		 echo $e; die;
	  }	
	  $this->load->template('fulltest/sample',$this->data);
	}
	

}
