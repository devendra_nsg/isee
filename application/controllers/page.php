<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Copyright:GlobalSoftTree
* Author:Devendra
*/

class Page extends CI_Controller {
    
	
	public function __construct(){		
	   parent::__construct();
			
	   $this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(1));
	    $this->load->model('block_model');
		$this->load->model('cms_model');
	   
	   $this->data['law'] = $this->block_model->get_block(2); 
	}
	
	
	
	public function privacy()
	{   
	    $this->data['query']=$this->cms_model->get_cms(3);
		$this->load->template('cms/page',$this->data);
	}

    public function terms()
	{   
	    $this->data['query']=$this->cms_model->get_cms(4);
		$this->load->template('cms/page',$this->data);
	}
	
	public function advices()
	{   
	    $this->data['query']=$this->cms_model->get_cms(2);
		$this->load->template('cms/page',$this->data);
	}
	

}
