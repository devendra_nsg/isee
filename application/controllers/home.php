<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Index Page for this controller.
* Copyright:GlobalSoftTree
* Author:Devendra
*/

class Home extends CI_Controller {
    
	
	public function __construct(){		
	   parent::__construct();
			
	   $this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(1));
	   
	   $this->load->model('videos_model');
	   $this->load->model('block_model');
	   $this->data['law'] = $this->block_model->get_block(2); 
	}
	
	
	
	public function index()
	{
	  try{
		  $this->data['query']['masterIsee'] = $this->block_model->get_block(6);	// pass according to content needed on front end
		  $this->data['query']['instruction'] = $this->block_model->get_block(5);	
		  $this->data['query']['videos'] = $this->videos_model->get_all_videos();
		  
	  }catch (Exception $e){
		  
		 echo $e; die;
	  }	
	  $this->load->template('home',$this->data);
	}



}
