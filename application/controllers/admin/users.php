<?php
class Users extends CI_Controller{

	function __construct(){		
			parent::__construct();
			
			if(!$this->session->userdata('logged_in')){
				redirect('admin/login','refresh');
			}   
			 
			$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
			$this->data['modulename'] = ucfirst($this->uri->segment(2));
			$this->data['act'] = ucfirst($this->uri->segment(3));
			
			$this->load->model('user_model');		
		}
	
	
	public function index()
	{
	
		$this->data['query']=$this->user_model->get_all_users();
		$this->load->view('admin/user/manage_users',$this->data);
	
	}
	
	public function add()
	{
		if($_POST){
		    $config['upload_path'] = './public/image_gallery/user_thumb';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '1000';

			$this->load->library('upload', $config);
			$this->upload->do_upload();
			//$error = array('error' => $this->upload->display_errors());
			//print_r($error);die;
			$data_image = $this->upload->data(); //print_r($data_image);die;
			resize_function($data_image);
			$_POST['image']=$data_image['file_name'];
			
		   $this->data['id'] = $this->user_model->insert_user($_POST);
		   if($this->data['id']){
		      $this->session->set_flashdata('success', '1 Record Added!');
		      redirect("admin/users");
		   }
		}
		$this->data['memberships'] = $this->user_model->getAllMembership();
		$this->load->view('admin/user/add_user', $this->data);
	
	}
	
	public function edit()
	{
	     $id = $this->uri->segment(4); 
		 
		
	     if($_POST){
		       if($_FILES['userfile']['name']){ 
					$config['upload_path'] = './public/image_gallery/user_thumb';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '1000';
					
					$this->load->library('upload', $config);
					$this->upload->do_upload();
					//$error = array('error' => $this->upload->display_errors());
					//print_r($error);die;
					$data_image = $this->upload->data(); //print_r($data_image);die;
					resize_function($data_image);
					$_POST['image']=$data_image['file_name'];
					if(file_exists("./public/image_gallery/user_thumb/".$_POST['userfile_old'])) {
						unlink("./public/image_gallery/user_thumb/".$_POST['userfile_old']);
					}
				} else {
				    $_POST['image']=$_POST['userfile_old'];
				}
				unset($_POST['userfile_old']);
		   if($this->user_model->update_user($_POST)){
		      $this->session->set_flashdata('success', 'Record Updated!');   
		   }
		   redirect("admin/users/edit/".$_POST['id']);
		}
		
		$this->data['query'] = $this->user_model->get_user($id);
		$this->data['memberships'] = $this->user_model->getAllMembership();
	    $this->load->view('admin/user/edit_user',$this->data);
	
	}



}
