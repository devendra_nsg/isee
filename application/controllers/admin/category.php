<?php

class Category extends CI_Controller{
	
	
	function __construct(){		
		parent::__construct();

		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('category_model');					
	}
	
	
	
	public function index(){
	
		$this->data['query'] = $this->category_model->get_all_category();
		$this->load->view('admin/categories/manage_category',$this->data);
	}
	
	
	public function add(){ 
		
		if(!$_POST){
			$this->load->view('admin/categories/add_category',$this->data);		
		} else { 
			//print_r($_POST); die;
			$this->category_model->insert_category($_POST);
			redirect("admin/category");
				
		}
	}
		
	
	
	public function edit(){
		$id = $this->uri->segment(4);	//echo $id;
		$this->data['query']= $this->category_model->get_category($id);	
		if(!$_POST){
			  $this->load->view('admin/categories/edit_category',$this->data);
		} else { 
		      $this->category_model->update_category($_POST); 
			  $this->session->set_flashdata('success', 'Record Updated!');
		      redirect("admin/category/edit/".$_POST['id']);
		}		
		
	}
		
}

