<?php
class Settings extends CI_Controller
{
	function __construct(){		
		parent::__construct();
			
		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('setting_model');		
	}
	
	function account(){
		$this->data['veryfy']=1;
		$this->data['error']='';
		$this->data['success']='';
		
		$this->data['admin_email'] = $this->setting_model->get_admin_details();
		
		if(!$_POST){		
			$this->load->view('admin/settings/change_account',$this->data);
		} else {
			if($_POST['veryfy']==1){
				if($this->setting_model->verify_password($_POST['password'])==FALSE)
				{
					$this->data['error']="You don't have permission to continue..";
					$this->load->view('admin/settings/change_account',$this->data);					
				}   else 
				    {
					$this->data['veryfy']=2;
					$this->load->view('admin/settings/change_account',$this->data);
				    }
				
			 }elseif($_POST['veryfy']==2){
				    
					if($_FILES['userfile']['name']){ 
						$config['upload_path'] = './public/image_gallery/user_thumb';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size']	= '1000';
						
						$this->load->library('upload', $config);
						$this->upload->do_upload();
						//$error = array('error' => $this->upload->display_errors());
						//print_r($error);die;
						$data_image = $this->upload->data(); //print_r($data_image);die;
						resize_function($data_image);
					    $_POST['image']=$data_image['file_name'];
					}else{
					    $_POST['image']=$_POST['userfile_old'];
					}
					unset($_POST['userfile_old']);
				
				if($this->setting_model->update()){
				     $this->data['success']='Your information Successfully Updated.';
				}else{
				     $this->data['error']='Information not updated due to some error!.';
				}	
				$this->load->view('admin/settings/change_account',$this->data);				
			    }
		}					
	}
	
	
	
	function password(){
		$admin_arr = $this->session->userdata('logged_in');
		if($admin_arr['username']=='admin'){
			$this->data['error']='';
			$this->data['success']='';
		
			if(!$_POST){
					$this->load->view('admin/settings/change_password',$this->data);				
			} else { 
					$this->form_validation->set_rules('oldpass', '<strong>Old Password</strong>', 'required');
					$this->form_validation->set_rules('newpass', '<strong>New Password</strong>', 'required');
					if ($this->form_validation->run() == FALSE)
					{
					  $this->data['error']=validation_errors();
					  $this->load->view('admin/settings/change_password',$this->data);	
					}else{
				 
						if($this->setting_model->verify_password($_POST['oldpass'])==FALSE){
							$this->data['error'] = 'Incorrect Password Entered!';
							
						} elseif($_POST['newpass']!=$_POST['cpass']) {
							$this->data['error'] = 'Password Entered Mismatched!';
							
						} else{
							$this->setting_model->update_password($_POST['newpass']);
							$this->data['success']='Password Update successfull.';
							
						}
					
						$this->load->view('admin/settings/change_password',$this->data);	
					}
			}
		} else {
			redirect("admin/login","refresh");
		}
	}
	
	
	
	
	
	
}