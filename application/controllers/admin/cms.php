<?php

class Cms extends CI_Controller{
	
	public function __construct(){		

		parent::__construct();

		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));

		$this->load->model('cms_model');				
	}
	
	public function index(){ 
		$this->data['query'] = $this->cms_model->get_all_cms();
		$this->load->view('admin/cms/manage_cms',$this->data);
	}
	
	public function add(){	
		
		if(!$_POST){
			$this->load->view('admin/cms/add_cms',$this->data);	
		}else{
			$this->cms_model->add_cms($_POST);
			redirect('admin/cms');	
		}
		
	}
	
	
	public function view(){
	
		$id = $this->uri->segment(5); //echo $id;
		$this->data['res'] = $this->cms_model->get_cms($id);
		$this->load->view('admin/cms/view_cms',$this->data);			
	}
	
		
	public function edit(){
	
		 $id = $this->uri->segment(4);	//echo $id; die;
		 $this->data['query'] = $this->cms_model->get_cms($id);	
		if(!$_POST){
			$this->load->view('admin/cms/edit_cms',$this->data);
		} else { 
		    $this->cms_model->update_cms($_POST);
			$this->session->set_flashdata('success', 'Record Updated!');
		    redirect("admin/cms/edit/".$_POST['id']);
		}		
		
	}
		
		
}
