<?php

class Subcategory extends CI_Controller{
	
	function __construct(){		
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('subcategory_model');	
		$this->load->model('category_model');
						
	}
	
	public function index(){
		$this->data['query']=$this->subcategory_model->get_all_subcategory();
		$this->load->view('admin/subcategories/manage_subcategory',$this->data);
	}
	
	public function add(){ 
		$this->data['cate'] = $this->category_model->get_all_category();
		
		if(!$_POST){
			$this->load->view('admin/subcategories/add_subcategory',$this->data);		
		} else { 
			$this->form_validation->set_rules('cate_id', '<strong>Category</strong>', 'required');
			if($this->form_validation->run() == FALSE){
				$this->data['error']=validation_errors();
				$this->load->view('admin/subcategories/add_subcategory',$this->data);
			} else {
				$this->subcategory_model->insert_subcategory($_POST);
				$this->session->set_flashdata('success', '1 Record Added!');
				redirect("admin/subcategory");
			}
				
		}
	}
		
	
	
	//Loading user view(edit user form) and selecting data from table
	public function edit(){
		$id = $this->uri->segment(4);	//echo $id; die;
		$this->data['cate']=$this->category_model->get_all_category();
		$this->data['query']=$this->subcategory_model->get_subcategory($id);
		
		if(!$_POST){
			$this->load->view('admin/subcategories/edit_subcategory',$this->data);
		} else { 
			$this->subcategory_model->update_subcategory();
			$this->session->set_flashdata('success', 'Record Updated!');
			redirect("admin/subcategory/edit/".$_POST['id']);
		}		
		
	}
		
}


?>