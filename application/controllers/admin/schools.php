<?php

class Schools extends CI_Controller{
	
	
	function __construct(){		
		parent::__construct();

		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('school_model');					
	}
	
	
	
	public function index(){
	
		$this->data['query'] = $this->school_model->get_all_school();
		$this->load->view('admin/schools/manage_school',$this->data);
	}
	
	
	public function add(){ 
		
		if(!$_POST){
			$this->load->view('admin/schools/add_school',$this->data);		
		} else { 
			//print_r($_POST); die;
			$this->school_model->insert_school($_POST);
			redirect("admin/schools");
				
		}
	}
		
	
	
	public function edit(){
		$id = $this->uri->segment(4);	//echo $id;
		$this->data['query']= $this->school_model->get_school($id);	
		if(!$_POST){
			  $this->load->view('admin/schools/edit_school',$this->data);
		} else { 
		      $this->school_model->update_school($_POST); 
			  $this->session->set_flashdata('success', 'Record Updated!');
		      redirect("admin/schools/edit/".$_POST['id']);
		}		
		
	}
		
}

