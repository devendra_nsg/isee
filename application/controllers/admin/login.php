<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct(){
  	 parent::__construct();
		
		 $this->data['title'] = $this->config->item('site_name').' | Login';
		 $this->data['modulename'] = ucfirst($this->uri->segment(2));
		 
		 $this->load->model('login_model','',TRUE);		 
  }
	
	public function index()
	{ 		
		if(!$this->session->userdata('logged_in')){	
			$this->load->view('admin/login',$this->data);		
		} else {
			redirect('admin/dashboard');
		}
	}
	
	public function verify(){ //echo 'ram'; //die;
			
			$this->form_validation->set_error_delimiters( '', '<br>' );
			
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
			
			$this->form_validation->set_message('username', 'The Field <strong>Username</strong> is required');
			$this->form_validation->set_message('password', 'The Field <strong>Password</strong> is required');
		 
			 if($this->form_validation->run('login_form') ){
					redirect('admin/dashboard', 'refresh');
			 }else{
				 $this->data['error']=	validation_errors();
				 $this->load->view('admin/login',$this->data);
			 }
		 
		
	}
	
	function check_database($password)
	 {
	   //Field validation succeeded.  Validate against database
	   $username = $this->input->post('username');
	   $remember = $this->input->post('remember');
	 
	   //query the database
	   $result = $this->login_model->admin_login($username, $password);
	 
		   if($result)
		   {
				$sess_array = array(
				'id' => $result->id,
				'username' => $result->username
				);
				$this->session->set_userdata('logged_in', $sess_array);
					if ($remember == 1) {
					$this->load->helper('cookie');
					$cookie = array(
						'name' => 'email_cookie',
						'value' => $username,
						'expire' => '1209600'
					);
					set_cookie($cookie);
					$cookie2 = array(
						'name' => 'pass_cookie',
						'value' => $password,
						'expire' => '1209600'
					);
					set_cookie($cookie2);
					}
				
		   }
		   else
		   {
			 $this->form_validation->set_message('check_database', 'Invalid Username or Password');
			 return false;
		   }
	 }
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('admin/login','refresh');
	}
		
	
	
	 public function forgetpassword()
    {
        $this->form_validation->set_error_delimiters( '', '<br>' );
			if (!$_POST) {
				$this->load->view('admin/login');
			} else {
				$this->form_validation->set_rules('email', 'Email', 'required');
				if ($this->form_validation->run() == false) {
					$this->data['error'] = validation_errors();
					
					$this->load->view('admin/login', $this->data);
				}
				
				else {
					$email  = $this->input->post('email');
					$result = $this->login_model->check_admin_email($email);
					
					if ($result) {
						$sess_array = array(
							'id' => $result->id,
							'email' => $result->email,
							'password' => $result->password
						);
						$data = $this->login_model->get_email();
						$from = $data->email;
						
						//print_r($sess_array);die;
						
						//email section
						$this->email->set_newline("\r\n");
						
						$this->email->from($from);
						$this->email->to($sess_array['email']);
						$this->email->subject('Recover Your Password');
						$this->email->message('Account Login Info 
			Your Password is ' . $sess_array['password'] . ' and email address ' . $sess_array['email']);
						
						//$result = $this->email->send();
						//echo $this->email->print_debugger();
						$this->session->set_flashdata('message', 'Your Account Login Detail Had Been Sent Successfully To Your Email');
						redirect('admin/login');
					} else {
						
						$this->session->set_flashdata('error', 'Sorry No Matching Email Found');
						$this->load->view('admin/login');
						redirect('admin/login');
					}
					
					
					
				}
				
			}
    }
		
		
		
}

