<?php

class Section extends CI_Controller{
	
	
	function __construct(){		
		parent::__construct();

		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('section_model');					
	}
	
	
	
	public function index(){
	
		$this->data['query'] = $this->section_model->get_all_section();
		$this->load->view('admin/section/manage_section',$this->data);
	}
	
	
	public function add(){ 
		
		if(!$_POST){
			$this->load->view('admin/section/add_section',$this->data);		
		} else { 
			//print_r($_POST); die;
			$this->section_model->insert_section($_POST);
			redirect("admin/section");
				
		}
	}
		
	
	
	public function edit(){
		$id = $this->uri->segment(4);	//echo $id;
		$this->data['query']= $this->section_model->get_section($id);	
		if(!$_POST){
			  $this->load->view('admin/section/edit_section',$this->data);
		} else { 
		      $this->section_model->update_section($_POST); 
			  $this->session->set_flashdata('success', 'Record Updated!');
		      redirect("admin/section/edit/".$_POST['id']);
		}		
		
	}
		
}

