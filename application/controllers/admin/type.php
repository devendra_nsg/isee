<?php

class Type extends CI_Controller{
	
	
	function __construct(){		
		parent::__construct();

		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('type_model');					
	}
	
	
	
	public function index(){
	
		$this->data['query'] = $this->type_model->get_all_type();
		$this->load->view('admin/type/manage_type',$this->data);
	}
	
	
	public function add(){ 
		
		if(!$_POST){
			$this->load->view('admin/type/add_type',$this->data);		
		} else { 
			//print_r($_POST); die;
			$this->type_model->insert_type($_POST);
			redirect("admin/type");
				
		}
	}
		
	
	
	public function edit(){
		$id = $this->uri->segment(4);	//echo $id;
		$this->data['query']= $this->type_model->get_type($id);	
		if(!$_POST){
			  $this->load->view('admin/type/edit_type',$this->data);
		} else { 
		      $this->type_model->update_type($_POST); 
			  $this->session->set_flashdata('success', 'Record Updated!');
		      redirect("admin/type/edit/".$_POST['id']);
		}		
		
	}
		
}

