<?php
class Learningresource extends CI_Controller{
	
	function __construct(){		
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('learningresource_model');	
		$this->load->model('subcategory_Model');
		$this->load->model('category_Model');
		$this->load->model('level_model');
		$this->load->model('type_model');
						
	}
	
	public function index(){
		$this->data['query']=$this->learningresource_model->get_all_question();
		$this->load->view('admin/learningresource/manage_question',$this->data);
	}
	
	public function add(){ 
		$this->data['cate'] = $this->category_Model->get_all_category();
		$this->data['subcat'] = $this->subcategory_Model->get_all_subcategory();
		$this->data['level'] = $this->level_model->get_all_level();
		$this->data['type'] = $this->type_model->get_all_type();
			
		if(!$_POST){
			$this->load->view('admin/learningresource/add_question',$this->data);		
		} else { 
			$this->learningresource_model->insert_question($_POST);
			$this->session->set_flashdata('success', '1 Record Added!');
			redirect("admin/learningresource");	
		}
	}
	
	public function subjectUpdateByTopic() {
		if(isset($_POST['category_id'])) {
			$data = $this->subcategory_Model->getSubcategoryByTopicId($_POST['category_id']);
			echo json_encode($data);
		}
		exit;
	}
	
	
	//Loading user view(edit user form) and selecting data from table
	public function edit(){
		$id = $this->uri->segment(4);	//echo $id; die;
		$this->data['cate'] = $this->category_Model->get_all_category();
		$this->data['subcat'] = $this->subcategory_Model->get_all_subcategory();
		$this->data['level'] = $this->level_model->get_all_level();
		$this->data['type'] = $this->type_model->get_all_type();
		$this->data['query']=$this->learningresource_model->get_question($id);
		
		if(!$_POST){
			   $this->load->view('admin/learningresource/edit_question',$this->data);
		} else { 
			   if($this->learningresource_model->update_question($_POST)){
			   $this->session->set_flashdata('success', 'Record Updated!');
			   redirect("admin/learningresource/edit/".$_POST['id']);
			   }
		}		
		
	}
  

   public function importcsv()
    {  
	    if(!empty($_FILES['csvfile']['name'])){ 
		           	 
			//$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');
			//$this->form_validation->set_rules('csvfile', 'CSV File', 'trim|required');
			
			$this->load->library('csvreader');
			
				 // if ($this->form_validation->run() == FALSE) {
					  
			    //    echo $_FILES['csvfile']['name'];die;
				//    $this->data['error'] = validation_errors();
				    
				//}else{
					//$result =   @$this->csvreader->parse_file('/..'/BASEPATH.'public/csv/questions.csv');//path to csv file
					$result =   $this->csvreader->parse_file($_FILES['csvfile']['tmp_name']);//path to csv file
					//print_r($result);die;
					  foreach($result as $row){
						$this->learningresource_model->insert_question_csv($row);
					  }	
                      $this->session->set_flashdata('success', 'Records Updated!');
                      redirect('admin/learningresource/importcsv');					  
				//	}  
		}
		
		$this->load->view('admin/learningresource/importcsv',$this->data); 
     }


  
}


