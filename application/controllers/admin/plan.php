<?php
class plan extends CI_Controller
{
	function __construct(){		
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
			$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
			$this->data['modulename'] = ucfirst($this->uri->segment(2));
			$this->data['act'] = ucfirst($this->uri->segment(3));
			
			$this->load->model('plan_model');				
       }

		public function index()
		{
			$this->data['query']=$this->plan_model->get_all_plans();
			//print_r($this->data);
			$this->load->view('admin/plan/manage_plan',$this->data);
		}


		public function view()
		{
			$id=$this->uri->segment(4);
			$this->data['data'] = $this->plan_model->get_plan($id);
			$this->load->view('admin/plan/view_plan',$this->data);
		
		}




         public function edit(){
	      
			 $id = $this->uri->segment(4);	//echo $id;
			 $this->data['query'] = $this->plan_model->get_plan($id);
			
			 //print_r($this->data['query']);die;
			if(!$_POST){
				  $this->load->view('admin/plan/edit_plan',$this->data);
			  }else{
					if($this->plan_model->update_plan($_POST)){
					 $this->session->set_flashdata('success', 'Record Updated!');
					}else{
					 $this->session->set_flashdata('error','Some error!');
					}
				    redirect('admin/plan/edit/'.$_POST['id']);
				}
		 }		
	
	












}


?>