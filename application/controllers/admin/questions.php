<?php
class Questions extends CI_Controller{
	
	function __construct(){		
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('question_model');	
		$this->load->model('level_model');
		$this->load->model('section_model');
		$this->load->model('exam_model');
		
						
	}
	
	public function index(){
		$this->data['query']=$this->question_model->get_all_question();
		$this->load->view('admin/questions/manage_question',$this->data);
	}
	
	public function add(){ 
		
		$this->data['level'] = $this->level_model->get_all_level();
		$this->data['section'] = $this->section_model->get_all_section();
		$this->data['exam'] = $this->exam_model->get_all_exam();
			
		if(!$_POST){
			        $this->load->view('admin/questions/add_question',$this->data);		
		} else { 
					$this->question_model->insert_question($_POST);
					$this->session->set_flashdata('success', '1 Record Added!');
					redirect("admin/questions");	
		}
	}
		
	
	
	//Loading user view(edit user form) and selecting data from table
	public function edit(){
		$id = $this->uri->segment(4);	//echo $id; die;
		$this->data['level'] = $this->level_model->get_all_level();
		$this->data['section'] = $this->section_model->get_all_section();
		$this->data['exam'] = $this->exam_model->get_all_exam();
		$this->data['query']=$this->question_model->get_question($id);
		
		if(!$_POST){
			$this->load->view('admin/questions/edit_question',$this->data);
		} else { 
			$this->question_model->update_question($_POST);
			$this->session->set_flashdata('success', 'Record Updated!');
			redirect("admin/questions/edit/".$_POST['id']);
		}		
		
	}
	
	
	
	public function importcsv()
    {  
	    if(!empty($_FILES['csvfile']['name'])){ 
		           	 
			//$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');
			//$this->form_validation->set_rules('csvfile', 'CSV File', 'trim|required');
			
			$this->load->library('csvreader');
			
				 // if ($this->form_validation->run() == FALSE) {
					  
			    //    echo $_FILES['csvfile']['name'];die;
				//    $this->data['error'] = validation_errors();
				    
				//}else{
					//$result =   @$this->csvreader->parse_file('/..'/BASEPATH.'public/csv/questions.csv');//path to csv file
					$result =   $this->csvreader->parse_file($_FILES['csvfile']['tmp_name']);//path to csv file
					  foreach($result as $row){
						$this->question_model->insert_question_csv($row);
					  }	
                      $this->session->set_flashdata('success', 'Records Updated!');
                      redirect('admin/questions/importcsv');					  
				//	}  
		}
		
		$this->load->view('admin/questions/importcsv',$this->data); 
     }
	
		
}


