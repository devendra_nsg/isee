<?php

class Level_resource extends CI_Controller{
	
	
	function __construct(){		
		parent::__construct();

		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('level_resource_model');					
	}
	
	
	
	public function index(){
	
		$this->data['query'] = $this->level_resource_model->get_all_level_resource();
		$this->load->view('admin/level_resource/manage_level_resource',$this->data);
	}
	
	
	public function add(){ 
		
		if(!$_POST){
			$this->load->view('admin/level_resource/add_level_resource',$this->data);		
		} else { 
			//print_r($_POST); die;
			$this->level_resource_model->insert_level_resource($_POST);
			redirect("admin/level_resource");
				
		}
	}
		
	
	
	public function edit(){
		$id = $this->uri->segment(4);	//echo $id;
		$this->data['query']= $this->level_resource_model->get_level_resource($id);	
		if(!$_POST){
			  $this->load->view('admin/level_resource/edit_level_resource',$this->data);
		} else { 
		      $this->level_resource_model->update_level_resource($_POST); 
			  $this->session->set_flashdata('success', 'Record Updated!');
		      redirect("admin/level_resource/edit/".$_POST['id']);
		}		
		
	}
		
}

