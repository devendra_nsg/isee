<?php

class Powerdownload extends CI_Controller{
	
	
	function __construct(){		
		parent::__construct();

		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('powerdownload_model');					
	}
	
	
	
	public function index(){
	
		$this->data['query'] = $this->powerdownload_model->get_all_powerdownload();
		$this->load->view('admin/powerdownload/manage_powerdownload',$this->data);
	}
	
	
	public function add(){ 
		
		if($_POST){
			$config['upload_path'] = './public/pdf';
			$config['allowed_types'] = 'gif|jpg|png|pdf';
			$config['max_size']	= '1000';

			$this->load->library('upload', $config);
			$this->upload->do_upload('pdffile');
			//$error = array('error' => $this->upload->display_errors());
			//print_r($error);die;
			$data_image = $this->upload->data(); //print_r($data_image);die;
			//resize_function($data_image);
			$_POST['pdffile']=$data_image['file_name'];
			$this->data['id'] = $this->powerdownload_model->insert_powerdownload($_POST);
			 if($this->data['id']){
		      $this->session->set_flashdata('success', '1 Record Added!');
		      redirect("admin/powerdownload");
		   }	
		} 
		$this->load->view('admin/powerdownload/add_powerdownload',$this->data);	
	}
		
	
	
	public function edit(){
		$id = $this->uri->segment(4);	//echo $id;
		
		if($_POST){ //print_r($_FILES);die;
		       if($_FILES['pdffile']['name']){ 
					$config['upload_path'] = './public/pdf';
					$config['allowed_types'] = 'gif|jpg|png|pdf';
					$config['max_size']	= '1000';
					
					$this->load->library('upload', $config);
					$this->upload->do_upload('pdffile');
					//$error = array('error' => $this->upload->display_errors());
					//print_r($error);die;
					$data_image = $this->upload->data(); //print_r($data_image);die;
					//resize_function($data_image);
					$_POST['pdffile']=$data_image['file_name'];
					if(file_exists("./public/pdf/".$_POST['pdffile_old'])) {
						unlink("./public/pdf/".$_POST['pdffile_old']);
					}
				} else {
				    $_POST['pdffile']=$_POST['pdffile_old'];
				}
				unset($_POST['pdffile_old']);
		   if($this->powerdownload_model->update_powerdownload($_POST)){
		      $this->session->set_flashdata('success', 'Record Updated!');   
		   }
		   redirect("admin/powerdownload/edit/".$_POST['id']);
		}
		
		$this->data['query'] = $this->powerdownload_model->get_powerdownload($id);
	    $this->load->view('admin/powerdownload/edit_powerdownload',$this->data);	
		
	}
		
}

