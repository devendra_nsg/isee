<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct(){
  	parent::__construct();	
		 
		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		
		$this->load->model('dashboard_model');
							
	}
	
	function index(){
		$this->data['total_users'] = $this->dashboard_model->countUsers();
		$this->load->view('admin/dashboard', $this->data);		
	}
		
}