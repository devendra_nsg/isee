<?php

class Block extends CI_Controller{
	
	public function __construct(){		

		parent::__construct();

		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));

		$this->load->model('block_model');				
	}
	
	public function index(){ 
		$this->data['query'] = $this->block_model->get_all_block();
		$this->load->view('admin/block/manage_block',$this->data);
	}
	
	public function add(){	
		
		if(!$_POST){
			$this->load->view('admin/block/add_block',$this->data);	
		}else{
			$this->block_model->add_block($_POST);
			redirect('admin/block');	
		}
		
	}
	
	
	public function view(){
	
		$id = $this->uri->segment(5); //echo $id;
		$this->data['res'] = $this->block_model->get_block($id);
		$this->load->view('admin/block/view_block',$this->data);			
	}
	
		
	public function edit(){
	
		 $id = $this->uri->segment(4);	//echo $id; die;
		 $this->data['query'] = $this->block_model->get_block($id);	
		if(!$_POST){
			$this->load->view('admin/block/edit_block',$this->data);
		} else { 
		    $this->block_model->update_block($_POST);
			$this->session->set_flashdata('success', 'Record Updated!');
		    redirect("admin/block/edit/".$_POST['id']);
		}		
		
	}
		
		
}
