<?php

class Exam extends CI_Controller{
	
	function __construct(){		
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('exam_model');	
		$this->load->model('category_model');
						
	}
	
	public function index(){
		$this->data['query']=$this->exam_model->get_all_exam();
		$this->load->view('admin/exam/manage_exam',$this->data);
	}
	
	public function add(){ 
		$this->data['cate'] = $this->exam_model->get_all_exam();
		
		if(!$_POST){
			$this->load->view('admin/exam/add_exam',$this->data);		
		} else { 
		
		
		   $this->form_validation->set_rules('exam_name', '<strong>Category</strong>', 'required');
			if($this->form_validation->run() == FALSE){
			 
				$this->data['error']=validation_errors();
				$this->load->view('admin/exam/add_exam',$this->data);
			} else {
				$this->exam_model->insert_exam($_POST);
				$this->session->set_flashdata('success', '1 Record Added!');
				redirect("admin/exam");
			}
				
		}
	}
		
	
	
	//Loading user view(edit user form) and selecting data from table
	public function edit(){
		$id = $this->uri->segment(4);	//echo $id; die;
		$this->data['cate']=$this->exam_model->get_all_exam();
		$this->data['query']=$this->exam_model->get_exam($id);
		
		if(!$_POST){
			$this->load->view('admin/exam/edit_exam',$this->data);
		} else { 
			$this->exam_model->update_exam();
			$this->session->set_flashdata('success', 'Record Updated!');
			redirect("admin/exam/edit/".$_POST['id']);
		}		
		
	}
		
}


?>