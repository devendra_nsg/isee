<?php
class Faqs extends CI_Controller{
	
	public function __construct(){		
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
			
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
			
	    $this->load->model('faqs_model');
	}
	
	public function index(){  
		$this->data['query'] = $this->faqs_model->get_all_faqs();
		$this->load->view('admin/faqs/manage_faq',$this->data);
	}
	
	public function add(){

		//$this->load->library('fckeditor');
		if(!$_POST){
			$this->load->view('admin/faqs/add_faq',$this->data);
		}else{
			$this->faqs_model->add_faq($_POST);
			redirect('admin/faqs');
		}	
	}
	
	public function view(){
		$id = $this->uri->segment(4); //echo $id;die;
		$this->data['res'] = $this->faqs_model->get_faq($id);
		$this->load->view('admin/faqs/view_faq',$this->data);			
	}
	
	
	  public function edit(){
	
		 $id = $this->uri->segment(4);	//echo $id; die;
		 $this->data['query'] = $this->faqs_model->get_faq($id);
			if(!$_POST){
				$this->load->view('admin/faqs/edit_faq',$this->data);
			} else { 
				$this->faqs_model->update_faq($_POST);
				$this->session->set_flashdata('success', 'Record Updated!');
				redirect("admin/faqs/edit/".$_POST['id']);
			}		
	   }
	
	public function delete(){
		$id = $this->uri->segment(4); //echo $id;die;
		$this->faqs_model->delete($id);
		redirect('admin/faqs','refresh');
	}
	
	
}
	