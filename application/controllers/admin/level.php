<?php

class Level extends CI_Controller{
	
	
	function __construct(){		
		parent::__construct();

		if(!$this->session->userdata('logged_in')){
			redirect('admin/login','refresh');
		}
		
		$this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('level_model');					
	}
	
	
	
	public function index(){
	
		$this->data['query'] = $this->level_model->get_all_level();
		$this->load->view('admin/level/manage_level',$this->data);
	}
	
	
	public function add(){ 
		
		if(!$_POST){
			$this->load->view('admin/level/add_level',$this->data);		
		} else { 
			//print_r($_POST); die;
			$this->level_model->insert_level($_POST);
			redirect("admin/level");
				
		}
	}
		
	
	
	public function edit(){
		$id = $this->uri->segment(4);	//echo $id;
		$this->data['query']= $this->level_model->get_level($id);	
		if(!$_POST){
			  $this->load->view('admin/level/edit_level',$this->data);
		} else { 
		      $this->level_model->update_level($_POST); 
			  $this->session->set_flashdata('success', 'Record Updated!');
		      redirect("admin/level/edit/".$_POST['id']);
		}		
		
	}
		
}

