<?php 
class Videos extends CI_Controller{

	public function __construct(){
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')){
		redirect('admin/login','refresh');
		}
		
	    $this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(2));	
		$this->data['modulename'] = ucfirst($this->uri->segment(2));
		$this->data['act'] = ucfirst($this->uri->segment(3));
		
		$this->load->model('videos_model');	
	}

	public function index(){
		$this->data['query']= $this->videos_model->get_all_videos();
		$this->load->view('admin/videos/manage_videos',$this->data);
	}

	 
	public function add(){
	 
		if(!$_POST){
			$this->load->view('admin/videos/add_video',$this->data);
		}else{
		
			$config['upload_path'] = './public/image_gallery/video_thumb';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '1000';

			$this->load->library('upload', $config);
			$this->upload->do_upload();
			//$error = array('error' => $this->upload->display_errors());
			//print_r($error);die;
			$data_image = $this->upload->data(); //print_r($data_image);die;
			resize_function($data_image);
			$_POST['video_img']=$data_image['file_name'];

			$this->videos_model->add_video($_POST);
			redirect('admin/videos');
		}
	 
	 }

	 
	 
	  public function edit(){
	
		   if(!$_POST){
		        $id = $this->uri->segment(4);
				$this->data['query'] = $this->videos_model->get_video($id); //print_r($data);die;
				$this->load->view('admin/videos/edit_video',$this->data);
		   }else{  
				if($_FILES['userfile']['name']){ 
					$config['upload_path'] = './public/image_gallery/video_thumb';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '1000';
					
					$this->load->library('upload', $config);
					$this->upload->do_upload();
					//$error = array('error' => $this->upload->display_errors());
					//print_r($error);die;
					$data_image = $this->upload->data(); //print_r($data_image);die;
					resize_function($data_image);
					$_POST['video_img']=$data_image['file_name'];
					if(file_exists('./public/image_gallery/video_thumb/'.$_POST['userfile_old'])) {
						unlink('./public/image_gallery/video_thumb/'.$_POST['userfile_old']);
					}
				}else{
				    $_POST['video_img']=$_POST['userfile_old'];
				 }
					   if($this->videos_model->update_video($_POST)){
					     $this->session->set_flashdata('success', 'Record Updated!');
					   }else{
					     $this->session->set_flashdata('error','Some error!');
					   }
				redirect('admin/videos/edit/'.$_POST['id']);
			}
	 
	 }



    public function view(){
		$id = $this->uri->segment(4); //echo $id;die;
		$this->data['query'] = $this->videos_model->get_video($id);
		$this->load->view('admin/videos/view_video',$this->data);			
	}

    public function delete(){
		$id = $this->uri->segment(4); //echo $id;die;
		$this->videos_model->delete($id);
		redirect('admin/videos','refresh');
	}









}
