<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Index Page for this controller.
* Copyright:GlobalSoftTree
* Author:Devendra
*/

class Contact extends CI_Controller {
    
	
	public function __construct(){		
	   parent::__construct();
			
	   $this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(1));
	   $this->load->model('block_model');
	   
	   $this->data['law'] = $this->block_model->get_block(2); 
	}
	
	
	
	public function index()
	{   
	    $this->data['query']['contact'] = $this->block_model->get_block(3);
		$this->load->template('contact',$this->data);
	}



}
