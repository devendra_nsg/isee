<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Copyright:GlobalSoftTree
* Author:Devendra
*/

class User extends CI_Controller {
    
	
	public function __construct(){		
	   parent::__construct();
	     
         //print_r($this->session->userdata('user_logged_in'));		 
	     $this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(1));
	     $this->load->model('block_model');
		 $this->load->model('login_model');
		  $this->load->model('user_model');
	     $this->data['law'] = $this->block_model->get_block(2); 
	}
	
	public function session_checker()
	{ 		
		if(!$this->session->userdata('user_logged_in')){	
			redirect('user/login');	
		} 
	}
	
	public function session_checker2()
	{ 		
		if($this->session->userdata('user_logged_in')){	
			redirect('user/account');	
		} 
	}
	
	
	 public function register()
	{   
	    $this->session_checker2();
		
	        $this->form_validation->set_error_delimiters( '', '<br>' );		
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|matches[confirmpassword]');
			$this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'trim|required|min_length[4]');
			$this->form_validation->set_rules('agreement', 'Agree', 'trim|required|xss_clean');
			
			$this->form_validation->set_message('agreement', 'Please agree <strong>terms & agreement</strong> to continue..');
		 
			 if($this->form_validation->run()!=false){
				 
				$email = $this->input->post('email'); 
				$password = $this->input->post('password');
				$username = $this->input->post('username');
				$email_checkpoint = $this->login_model->check_email($this->input->post('email'));
				
				if($email_checkpoint!='exists'){
					$data=$this->login_model->get_email_user();

					//// mailer
					$this->email->set_newline("\r\n");
					$from=$data['email'];
					$this->email->from($from);
					$this->email->to($email);  
					$this->email->subject('Registration Detail');  
					$this->email->message('Account Login Info Your Password is '.$password.' and email address '.$email);

					$this->email->send();
					$last_id=$this->login_model->insert_user($_POST);
						if(isset($last_id)){
							$session_array = array(
													'id' => $last_id,
													'email' => $email,
													'username' => $username,
													);
							$this->session->set_userdata('user_logged_in', $session_array);
						}
					$this->session->set_flashdata('success', 'Your Registration Has Been Successful, Please Check Your Email To Activate Your Account.');
					}else{
					$this->session->set_flashdata('error', 'Email Address Already Registered, Please Get Login.');	
					}
			    
				redirect('user/register',$this->data);
			 }else{
				
				 $this->data['error']=	validation_errors();
				 $this->load->template('user/register',$this->data);
			 }	 
		
	}
	
	
	
	public function login()
	{   
	    $this->session_checker2();
	    $this->form_validation->set_error_delimiters( '', '<br>' );
			
			$this->form_validation->set_rules('email', 'Email', 'email|trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
			
		 
			 if($this->form_validation->run('login_form') ){
					redirect();
			 }else{
				 $this->data['error']=	validation_errors();
				 $this->load->template('user/login',$this->data);
			 }
	}

	
	function check_database($password)
	 {
	   //Field validation succeeded.  Validate against database
	   $email = $this->input->post('email');
	   //$remember = $this->input->post('remember');
	 
	   //query the database
	   $result = $this->login_model->user_login($email, $password);
	 
		   if($result)
		   {
				$sess_array = array(
				'id' => $result->id,
				'username' => $result->username,
				'email' => $result->email
				);
				$this->session->set_userdata('user_logged_in', $sess_array);
					/*if ($remember == 1) {
					$this->load->helper('cookie');
					$cookie = array(
						'name' => 'email_cookie',
						'value' => $username,
						'expire' => '1209600'
					);
					set_cookie($cookie);
					$cookie2 = array(
						'name' => 'pass_cookie',
						'value' => $password,
						'expire' => '1209600'
					);
					set_cookie($cookie2);
					}*/
				
		   }
		   else
		   {
			 $this->form_validation->set_message('check_database', 'Invalid Email or Password');
			 return false;
		   }
	 }
	
	public function account()
	{   
	    $this->session_checker();
		$userData = $this->session->userdata('user_logged_in');
		$this->data['query'] = $this->user_model->get_user($userData['id']);
		$this->load->template('user/account',$this->data);
	}
	
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('user/login','refresh');
	}
	
	
	public function fblogin(){
		$this->session_checker2();
		@$this->load->library('facebook');
		
		$facebook = new Facebook(array(
		'appId'		=>  $this->config->item('appID'), 
		'secret'	=> $this->config->item('appSecret'),
		));
		$users = $facebook->getUser();

			if ($users!="") {	
			  try {

				$user_info = $facebook->api('/me');
				//print_r($user_info);
				$logoutUrl = $facebook->getLogoutUrl();
			    //print_r($logoutUrl);	die;
				
				$email_checkpoint = $this->login_model->check_email($user_info['email']);
				
				if($email_checkpoint!='exists'){
					
                    $auto_password = rand(000000,999999);    
					//// mailer
					$this->email->set_newline("\r\n");
					$from = $this->config->item('supportEmail');
					$this->email->from($from);
					$this->email->to($user_info['email']);  
					$this->email->subject('Registration Detail');  
					$this->email->message('Account Login Info Your Password is '.$auto_password.' and email address '.$user_info['email']);
					$this->email->send();
					
					$data = array('username'=>$user_info['first_name'],'email'=>$user_info['email'],'password'=>$auto_password);
					
					   $last_id=$this->login_model->insert_user($data);
						if(isset($last_id)){
							$sess_array = array(
												'id' => $last_id,
												'username' => $user_info['first_name'],
												'email' => $user_info['email'],
												'fbLogout' => $logoutUrl
												);
						    $this->session->set_userdata('user_logged_in', $sess_array);    
						}
				}else{
					//query the database
					$result = $this->login_model->user_login_fb($user_info['email']);
					if($result)
					{
							$sess_array = array(
												'id' => $result->id,
												'username' => $result->username,
												'email' => $result->email,
												'fbLogout' => $logoutUrl
												);
							$this->session->set_userdata('user_logged_in', $sess_array);  
					}		
				}
				redirect();
				
			  } catch (FacebookApiException $e) {
				$users = null;
			  }
			}
		$this->load->template('user/login',$this->data);
	}
	

}
