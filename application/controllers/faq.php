<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Index Page for this controller.
* Copyright:GlobalSoftTree
* Author:Devendra
*/

class Faq extends CI_Controller {
    
	
	public function __construct(){		
	   parent::__construct();
			
	   $this->data['title'] = $this->config->item('site_name').' | '.ucfirst($this->uri->segment(1));
	    $this->load->model('block_model');
		$this->load->model('faqs_model');
	   
	   $this->data['law'] = $this->block_model->get_block(2); 
	}
	
	
	
	public function index()
	{   
	    $this->data['query'] = $this->faqs_model->get_all_faqs();
		$this->load->template('faq',$this->data);
	}



}
