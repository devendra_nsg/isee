<?php 


    function get_admin_details(){
			
		 $CI =& get_instance();
		 $CI->load->model('setting_model');
		 return $CI->setting_model->get_admin_details();
	}
	
	
	
    function resize_function($upload_data){
	    $CI =& get_instance();
		$image_config["image_library"] = "gd2";
		$image_config["source_image"] = $upload_data["full_path"];
		$image_config['create_thumb'] = FALSE;
		$image_config['maintain_ratio'] = TRUE;
		$image_config['new_image'] = $upload_data["file_path"] . 'small/'.$upload_data["file_name"];
		$image_config['quality'] = "100%";
		$image_config['width'] = 250;
		$image_config['height'] = 150;
		@$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
		$image_config['master_dim'] = ($dim > 0)? "height" : "width";
		
		
		$CI->load->library('image_lib');
		$CI->image_lib->initialize($image_config);
		$CI->image_lib->resize();
		
		//// Resize code	
	}