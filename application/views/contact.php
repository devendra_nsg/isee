<div class="wrapper">
    <div class="container">
    	<h1>Get in Touch</h1>
    	<div class="block twoThird">
        	<div class="contactform">
            	<div class="contactheadtxt">
                <p class="contacthead">
                	<?php echo $query['contact']->description; ?>
                </div>
                <div class="contactfields"> 
                	<form>
                    	<p><label>First Name<small>add your first name</small></label><input type="text" /></p>
                        <p><label>Last Name<small>add your last name</small></label><input type="text" /></p>
                        <p><label>Email<small>add your email</small></label><input type="text" /></p>
                    	<p><label>Message<small>add your message</small></label><textarea></textarea></p>
                        <p><label>&nbsp;</label><input type="reset" value="Clear Form" /><input type="submit" value="Send Message" /></p>
                    </form>
                </div>
                
            </div>
        </div>
        <div class="block three cImg">
        	<img src="<?php echo base_url("public/images/contactimg.jpg")?>" />
        </div>
    </div>
    <?php $this->load->view('/templates/fb-fanbox.php'); ?>