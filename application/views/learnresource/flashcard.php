 <script src="<?php echo base_url("public/js/jquery.quickflip.source.js")?>"></script>
 <link href="<?php echo base_url("public/css/basic-quickflips.css")?>" rel="stylesheet" type="text/css">
 <div class="wrapper">
    <div class="container flashcard">
    	<span class="breadcrumb"><a class="backico" href="plate-ac-learning-level-flashcard-upper-level-topic-subject.php">Back</a> to Rome Review <?=$level['name']?>	-- Revised</span>
		
		
		
		
        <div class="leftarea">
        	<!--<div class="quesbox">
            	
                <img class="star" src="<?php echo base_url("public/images/star.png")?>" alt="" />
                <p class="flip"><a href="#" class="quickFlipCta">Click to flip</a></p>
            </div>-->
			
			
			<div class="quickFlip2">
				
				<div class="blackPanel">
					<h3 class="first quickFlipCta"></h3>

					<h3 id="your_question" style="font-size:25px;"><?php if(!empty($question[0]['question'])) { $page = 1; echo strip_tags($question[0]['question']);} else { $page = 1; echo "Question not updated"; }?></h3>
					
					<p class="flip"><a href="" class="quickFlipCta">Click to Flip</a></p>

				</div>
			
				<div class="redPanel">
					<h3 class="first quickFlipCta"></h3>
					<h4 style="font-size:25px;" id="your_answer"><?php if(!empty($question[0]['answer'])) { $page = 1; echo strip_tags($question[0]['answer']);} else { $page = 1; echo "Answer Not Available"; }?></h4>
				
					
					<p class="flip"><a href="" class="quickFlipCta">Click to Flip</a></p>
				</div>
			</div>
			
			<ul class="progressbar">
				<li class="navbtn"><a class="leftbtn disable" style="cursor:pointer;" id="<?=$page?>">prev</a></li>
				<li class="progress"><a><span><?php if(!empty($question[0]['question'])) {?><level id="page_from">1</level> of <level id="page_to"><?=count($question)?></level><?php } else {?>0 of 0 <?php }?></span><img src="<?php echo base_url("public/images/progressbar.png")?>" /></a></li>
				<li class="navbtn"><a class="rightbtn" style="cursor:pointer;" id="<?=$page?>">next</a></li>
			</ul>
        </div>
        <?php if(!empty($question)) {
				foreach($question as $key=>$question) {?>
					<div id="question-<?=$key+1?>" style="display:none;"><?=strip_tags($question['question'])?></div>
					<div id="answer-<?=$key+1?>" style="display:none;"><?=strip_tags($question['answer'])?></div>
		<?php }
			}?>
       <div class="rightColumn">
       <div class="uibtn hide">
       <span>Motion</span>
       <a class="active" href="#">Flip</a>
       <a href="#">Flow</a>
       </div>
       <div class="uibtn hide">
       <span>Audio</span>
       <a class="active" href="#">on</a>
       <a href="#">Off</a>
       <a class="sep">Advanced</a>
       </div>
       <div class="uibtn">
       <span>Start With</span>
       <a class="full active" href="#">on</a>
       <a class="full" href="#">Off</a>
       <a class="full">Advanced</a>
       </div>
       </div>
    </div>
  <script type="text/javascript">
$(function() {
    $('.quickFlip2').quickFlip();
});
</script>
  <script>
  $(document).ready(function(){
	 $(".rightbtn").on("click", function(){
		 var max_page = parseInt($("#page_to").html());
		 var next = parseInt($(this).attr("id"))+1;
		 if(next<=max_page) {
			 var next_question = $("#question-"+next).html();
			 var next_answer = $("#answer-"+next).html();
			 $("#your_answer").text(next_answer);
			 $("#your_question").text(next_question);
			 $(".leftbtn").attr("id", next);
			 $(".rightbtn").attr("id", next);
			 $("#page_from").text(next);
			 
			 var toMatch = "none";
			 var str = $(".blackPanel").attr("style");
			 var filter = new RegExp('\\b' + toMatch + '\\b', 'gi');
			 if(str.match(filter)) {
				$(".blackPanel").css({"display":"block"});
				$(".redPanel").css({"display":"none"});
			 }
		 }
	 })
	 $(".leftbtn").on("click", function(){
		 var next = parseInt($(this).attr("id"))-1;
		 if(next>=1) {
			 var next_question = $("#question-"+next).html();
			 var next_answer = $("#answer-"+next).html();
			 $("#your_answer").text(next_answer);
			 $("#your_question").text(next_question);
			 $(".leftbtn").attr("id", next);
			 $(".rightbtn").attr("id", next);
			 $("#page_from").text(next);
			 
			 var toMatch = "none";
			 var str = $(".blackPanel").attr("style");
			 var filter = new RegExp('\\b' + toMatch + '\\b', 'gi');
			 if(str.match(filter)) {
				$(".blackPanel").css({"display":"block"});
				$(".redPanel").css({"display":"none"});
			 }
		 }
	 })
  })
  </script>
  
 