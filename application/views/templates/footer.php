 <footer class="sitefooter">
    <div class="labelcontainer">
    	<h3 class="label caps">Get in touch</h3>
    </div>
    <div class="container">
    	<div class="block three">
        	<ul class="pagelinks">
            	<li><a href="<?php echo base_url("page/privacy")?>">Privacy Policy</a></li>
				<li><a href="<?php echo base_url("page/terms")?>">Terms of Use</a></li>
            </ul>
        </div>
        <div class="block three">
        	<h4>Follow us</h4>
            <ul class="socialLinks">
            	<li><a href="#"><img src="<?php echo base_url("public/images/facebook-icon.png")?>" alt="facebook-icon" /></a></li>
                <li><a href="#"><img src="<?php echo base_url("public/images/twitter-icon.png")?>" alt="twitter-icon" /></a></li>
                <li><a href="#"><img src="<?php echo base_url("public/images/google-icon.png")?>" alt="google-icon" /></a></li>
            </ul>
        </div>
        <div class="block three">
        	<div class="contactinfo">
            	<ul>
                	<li class="address"><strong>Address: </strong> Lo angeles, California 90046</li>
                    <li class="mail"><strong>mail: </strong> <a href="mailto:info@fasteducators.com">info@fasteducators.com</a></li>
                    <li class="phone"><strong>Phone: </strong> 9999999999</li>
                </ul>
            </div>
        </div>
    </div>
  
   <?php echo $law->description; ?>
   
    <div class="copyright textCenter">
    	&copy; Copyright <?=date('Y')?> @ fasteducators.com, All Rights Reserved
    </div>
    <div class="clear"></div>	
    </footer>
    </div>
</body>
</html>