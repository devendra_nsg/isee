<?php $sessionData = $this->session->userdata('user_logged_in');  //print_r($sessionData);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?></title>
<link rel="stylesheet" href="<?php echo base_url("public/css/style.css")?>" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("public/css/programmer.css")?>" type="text/css" />
<script src="<?php //echo base_url("public/js/jquery.validate.js")?>"></script>
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>

</head>

<body>
<div class="headerContainer">
	<header class="siteheader">
	<a class="sitelogo" href="<?php echo base_url("home")?>"><img src="<?php echo base_url("public/images/sitelogo.png")?>" alt="logo" /></a>

    <div class="boxContainer">
    <div class="headerLinks">
    	<div class="toplinks">
		    <?php if(empty($sessionData)){ ?>
        	<a href="<?php echo base_url("user/register")?>">Join Now</a>
            <a href="<?php echo base_url("user/login")?>">Login</a>
            <?php }else{ ?>
			<a href="<?php echo base_url("user/account")?>"><?=$sessionData['username']?></a>
            <a href="<?php echo base_url("user/logout")?>">Logout</a>
			<?php } ?>
		</div>
        <nav class="sitenavigation">
    	<ul>
        	<li><a href="<?php echo base_url("home")?>">Home</a></li>
			<li><a href="<?php echo base_url("membership")?>">Get Started</a></li>
            <li><a href="<?php echo base_url("school")?>">Schools</a></li>
			<li><a href="<?php echo base_url("page/advices")?>">Parents</a></li>
			<li><a href="<?php echo base_url("tips")?>">Tips & Tricks</a></li>
            <li><a href="<?php echo base_url("powerdownload")?>">Power Downloads</a></li>
			<li><a href="<?php echo base_url("#")?>">NewsLetter</a></li>
            <li><a href="<?php echo base_url("faq")?>">Faq</a></li>
            <li><a href="<?php echo base_url("team")?>">Team</a></li>
            <li><a href="<?php echo base_url("contact")?>">Contact Us</a></li>
        </ul>	
    </nav>
    </div>
    
    <div class="clear"></div>
    </div>
    <img class="headimg" src="<?php echo base_url("public/images/headerimage.png")?>" alt="tag Line" />

<div class="clear"></div>
</header>
</div>