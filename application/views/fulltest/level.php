<div class="wrapper">
    <div class="container categoryChoose">
    	
        <h2 class="caps textCenter">Choose Your Level</h2>
		
		<?php foreach($query['level']->result() as $row){ ?>
        <div class="block three">
        	<div class="tests textCenter">
            	<a href="<?php echo base_url('fulltest/exam/level/'.$row->id); ?>"> <span><?php echo $row->name; ?></span> <?php echo $row->description; ?></a>
            </div>
        </div>
        <?php } ?>
    	
    </div>
    
    
    <?php $this->load->view('/templates/fb-fanbox.php'); ?>