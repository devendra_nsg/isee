<div class="container">
   		<div class="TestHeader">
        	<h2 class="testTitle">ISEE Lower #1 <span>Section 1: Verbal Reasoning</span></h2>
       		<div class="testheaderRight">
            	<div class="Timedetails">
                	<span>Time Remaining: </span>
                    <div class="timeblock">
                    	<span>49</span> Min <span>41</span> Sec
                    </div>
                <div class="clear"></div>
                </div>
                <ul class="testbtns">
                	<li><a class="pause" href="#">Pause</a></li>
                    <li><a class="end" href="#">End</a></li>
                </ul>
            </div>
       <div class="clear"></div>
        </div>
        <div class="quesList">
        	<ul>
            	<li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">6</a></li>
                <li><a href="#">7</a></li>
                <li><a href="#">8</a></li>
                <li><a href="#">9</a></li>
                <li><a href="#">10</a></li>
                <li><a href="#">11</a></li>
                <li><a href="#">12</a></li>
                <li><a href="#">13</a></li>
                <li><a href="#">14</a></li>
                <li><a href="#">15</a></li>
                <li><a href="#">16</a></li>
                <li><a href="#">17</a></li>
                <li><a href="#">18</a></li>
                <li><a href="#">19</a></li>
                <li><a href="#">20</a></li>
                <li><a href="#">21</a></li>
                <li><a href="#">22</a></li>
                <li><a href="#">23</a></li>
                <li><a href="#">24</a></li>
                <li><a href="#">25</a></li>
                <li><a href="#">26</a></li>
                <li><a href="#">27</a></li>
                <li><a href="#">28</a></li>
                <li><a href="#">29</a></li>
                <li><a href="#">30</a></li>
                <li><a href="#">31</a></li>
                <li><a href="#">32</a></li>
                <li><a href="#">33</a></li>
                
            </ul>
        </div>
   		<div class="quesBody">
        	<div class="quesbodyLeft">
            	<h1>Instructions</h1>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a </p>
                <h2>Part one: Synonym</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
           <img src="<?php echo base_url('/public/images/screenshot1.png');?>" />
           <h2>Part two: Sentence Completion</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                <img src="<?php echo base_url('/public/images/screenshot2.png');?>" />
            </div>
            <div class="quesbodyRight">
            	<a class="rightNav" href="plate-ba-question-page-verbal-reasoning1.php">Start Test</a>
            	 <div class="clear"></div>
                
            </div>
        <div class="clear"></div>
        </div>
   </div>
  
    <?php $this->load->view('/templates/fb-fanbox.php'); ?>