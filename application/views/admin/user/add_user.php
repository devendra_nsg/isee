<?php $this->load->view("admin/templates/header-top.php"); ?>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<?php $this->load->view("admin/templates/header.php"); ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php $this->load->view("admin/templates/sidebar.php"); ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->  
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<?php $this->load->view("admin/templates/nav.php"); ?>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $modulename; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<form action="<?php echo base_url("admin/users/add"); ?>" method="post" name="form_admin" id="form_sample_2" class="form-horizontal" enctype="multipart/form-data" >
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				You have some form errors. Please check below.
			</div>
			<div class="alert alert-success hide">
				<button class="close" data-dismiss="alert"></button>
				Your form validation is successful!
			</div>
			<div class="control-group">
				<label class="control-label">Username<span class="required">*</span></label>
				<div class="controls">
					<input type="text" name="username" required data-required="1" class="span6 m-wrap"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">First Name<span class="required">*</span></label>
				<div class="controls">
					<input type="text" name="first_name" required data-required="1" class="span6 m-wrap"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Last Name</label>
				<div class="controls">
					<input type="text" name="last_name"  data-required="1" class="span6 m-wrap"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Email<span class="required">*</span></label>
				<div class="controls">
					<input name="email" type="text" required class="span6 m-wrap"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Password<span class="required">*</span></label>
				<div class="controls">
					<input name="password" type="password" required class="span6 m-wrap"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Profile Picture</label>
				<div class="controls">
					<input name="userfile" type="file"  class=""/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Status</label>
				<div class="controls">
					<select class="span6 m-wrap" name="status">
						<option value="0">Inactive</option>
						<option value="1">Active</option>
						<option value="2">Block</option>
					</select>
				</div>
			</div>
			<!--<div class="control-group">
				<label class="control-label">Chosen Dropdown<span class="required">*</span></label>
				<div class="controls chzn-controls">
					<select id="form_2_chosen" class="span6 chosen" data-with-diselect="1" name="options1" data-placeholder="Choose an Option" tabindex="1">
						<option value="">Select...</option>
						<option value="Option 1">Option 1</option>
						<option value="Option 2">Option 2</option>
						<option value="Option 3">Option 3</option>
						<option value="Option 4">Option 4</option>
					</select>
				</div>
			</div>-->
			<div class="control-group">
				<label class="control-label">Membership</label>
				<div class="controls">
					<?php if(!empty($memberships)) {
						foreach($memberships as $member) {?>
						<label class="radio line">
						<input type="radio" name="plan_id" value="<?=$member['id']?>" />
						<?=$member['duration']?>
						</label>
					<?php }
					}?> 
					<div id="form_2_membership_error"></div>
				</div>
			</div>
			<div class="form-actions">
				<button type="submit" class="btn green" >Save</button>
				<button type="button" class="btn">Cancel</button>
			</div>
		</form>
		<!-- END FORM-->
	</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->         
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->  
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php $this->load->view("admin/templates/footer.php"); ?>
	<!-- END FOOTER -->

<?php $this->load->view("admin/templates/footer-bottom.php"); ?>