<?php $this->load->view("admin/templates/header-top.php"); ?>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<?php $this->load->view("admin/templates/header.php"); ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php $this->load->view("admin/templates/sidebar.php"); ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->  
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<?php $this->load->view("admin/templates/nav.php"); ?>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $modulename; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<form action="<?php echo base_url('admin/learningresource/add'); ?>" method="post" name="form_admin" id="form_sample_2" class="form-horizontal">
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				You have some form errors. Please check below.
			</div>
			<div class="alert alert-success hide">
				<button class="close" data-dismiss="alert"></button>
				Your form validation is successful!
			</div>
			
			<div class="control-group">
				<label class="control-label">Type<span class="required">*</span></label>
				<div class="controls">
					<select name="type" id="type" required class="span6 m-wrap" >
					<option value="">Select Type</option>
					<? foreach($type->result() as $row){ ?>
					<option value="<?=$row->id?>"><?=$row->name?></option>
					<? }?>
					</select>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Level<span class="required">*</span></label>
				<div class="controls">
					<select name="level" required class="span6 m-wrap" >
					<option value="">Select Level</option>
					<? foreach($level->result() as $row){ ?>
					<option value="<?=$row->id?>"><?=$row->name?></option>
					<? }?>
					</select>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Topic<span class="required">*</span></label>
				<div class="controls">
					<select name="category" id="category" required class="span6 m-wrap" >
					<option value="">Select Topic</option>
					<? foreach($cate->result() as $row){ ?>
					<option value="<?=$row->id?>"><?=$row->name?></option>
					<? }?>
					</select>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Subject<span class="required">*</span></label>
				<div class="controls">
					<select name="subcategory" id="subject" required class="span6 m-wrap" >
					<option value="">Select Subject</option>
					<? foreach($subcat->result() as $row){ ?>
					<option value="<?=$row->id?>"><?=$row->name?></option>
					<? }?>
					</select>
				</div>
			</div>
			
			<div class="control-group title-hide">
				<label class="control-label">Title<span class="required">*</span></label>
				<div class="controls">
					<textarea id="title" name="title" required rows="2"></textarea>
				</div>
			</div>
			
			<div class="control-group question-hide">
				<label class="control-label">Question<span class="required">*</span></label>
				<div class="controls">
					<textarea class="span12 ckeditor m-wrap" id="question" name="question" required rows="6" data-error-container="#editor2_error"></textarea>
					<div id="editor2_error"></div>
				</div>
			</div>
			<div class="control-group answer-hide">
				<label class="control-label">Answer<span class="required">*</span></label>
				<div class="controls">
					<textarea class="span12 ckeditor m-wrap" id="answer" name="answer" required rows="6" data-error-container="#editor2_error"></textarea>
					<div id="editor2_error"></div>
				</div>
			</div>
			<div class="control-group video-hide">
				<label class="control-label">Video</label>
				<div class="controls">
					<input type="text" name="video"  data-required="1" class="span6 m-wrap" id="video"/>
				</div>
			</div>
			
			<div class="control-group pdf-file">
				<label class="control-label">PDF File</label>
				<div class="controls">
					<input type="file" name="file" />
				</div>
			</div>
			
			<div class="form-actions">
				<button type="submit" class="btn green" >Save</button>
				<button type="button" class="btn">Cancel</button>
			</div>
		</form>
		<!-- END FORM-->
	</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->         
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->  
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php $this->load->view("admin/templates/footer.php"); ?>
	<!-- END FOOTER -->

<?php $this->load->view("admin/templates/footer-bottom.php"); ?>
<script>
$(document).ready(function(){
	$("#category").on("change", function(){
		var category = $(this).val();
		if(category) {
			$.ajax({
				url:"subjectUpdateByTopic",
				type:"POST",
				data:"category_id="+category,
				dataType:"json",
				success:function(data){
					var option = '<option value="">Select Subject</option>';
					if(data[0]) {
						$.each(data, function(key, value){
							option += '<option value="'+value['id']+'">'+value['name']+'</option>';
						})
					}
					$("#subject").html(option);
				}
			})
		}
	})
	
	$("#type").on("change", function(){
		var val = $(this).val();
		if(val!=3) {
			$(".question-hide").css({"display":"block"});
			$(".answer-hide").css({"display":"block"});
			$(".video-hide").css({"display":"block"});
			$("#question").attr("required", true);
			$("#answer").attr("required", true);
			$("#video").attr("required", true);
			$(".pdf-file").css({"display":"none"});
		} else if(val==3){
			$(".question-hide").css({"display":"none"});
			$(".answer-hide").css({"display":"none"});
			$(".video-hide").css({"display":"none"});
			$("#question").attr("required", false);
			$("#answer").attr("required", false);
			$("#video").attr("required", false);
			$(".pdf-file").css({"display":"block"});
		}
		if(val==1) {
			$(".title-hide").css({"display":"none"});
			$(".video-hide").css({"display":"none"});
			$("#title").attr("required", false);
			$("#video").attr("required", false);
		} else {
			$(".title-hide").css({"display":"block"});
			$("#title").attr("required", true);
		}
		if(val==2) {
			
			$(".answer-hide").css({"display":"none"});
			$("#answer").attr("required", false);
			$(".video-hide").css({"display":"block"});
			$("#video").attr("required", true);
		}
	})
})
</script>