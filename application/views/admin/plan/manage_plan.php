<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php print($title);?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="<?=base_url()?>public/admin/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?=base_url()?>public/admin/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/admin/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" href="<?=base_url()?>public/admin/plugins/data-tables/DT_bootstrap.css" />
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<?php $this->load->view("admin/templates/header.php"); ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	<?php $this->load->view("admin/templates/sidebar.php"); ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->        
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<?php $this->load->view("admin/templates/manage-nav.php"); ?>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<div class="caption"><i class="icon-globe"></i>Managed <?php print($modulename);?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body">
						
						<?php if($this->session->flashdata('success')){	?>
						<div class="alert alert-success hide" style="display: block;">
						<button data-dismiss="alert" class="close"></button>
						<?php echo $this->session->flashdata('success');?>
						</div>
						<?php } ?>
						
						<?php if($this->session->flashdata('error')){	?>
						<div class="alert alert-error hide" style="display: block;">
						<button data-dismiss="alert" class="close"></button>
						<?php echo $this->session->flashdata('error');?>
						</div>
						<?php } ?>	
							
							
								<div class="table-toolbar">
									<div class="btn-group"><a href="<?php echo base_url('admin/plan/add'); ?>">
										<!--<button id="sample_editable_1_new" class="btn green">
										Add <?php print($modulename);?> <i class="icon-plus"></i>
										</button>--></a>
									</div>
									<div class="btn-group pull-right">
										<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu pull-right">
											<li><a href="#">Print</a></li>
											<li><a href="#">Save as PDF</a></li>
											<li><a href="#">Export to Excel</a></li>
										</ul>
									</div>
								</div>
								<table class="table table-striped table-bordered table-hover display" id="sample_1">
									<thead>
										<tr>
											
											<th>Duration</th>
											<th>Price</th>
											<th>Offer</th>
											
											<th class="hidden-480">Action</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($query->result() as $row){ ?>
										<tr class="odd gradeX">
											
											<td><?=$row->duration;?></td>
											<td><?=$row->price;?></td>
											<td><?=$row->offer;?></td>
											
											<td ><a href="<?php echo base_url('admin/plan/edit/'.$row->id); ?>">Edit</a></td>
										</tr>
									<?php } ?>		
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php $this->load->view("admin/templates/footer.php"); ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   <script src="<?=base_url()?>public/admin/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>public/admin/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="<?=base_url()?>public/admin/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="<?=base_url()?>public/admin/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>public/admin/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<?=base_url()?>public/admin/plugins/excanvas.min.js"></script>
	<script src="<?=base_url()?>public/admin/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="<?=base_url()?>public/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>public/admin/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="<?=base_url()?>public/admin/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>public/admin/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>public/admin/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>public/admin/plugins/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="<?=base_url()?>public/admin/plugins/data-tables/DT_bootstrap.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>public/admin/scripts/app.js"></script>
	<script src="<?=base_url()?>public/admin/scripts/table-managed.js"></script>     
		<script>
	jQuery(document).ready(function() {   
	
	$('table.display').dataTable();
	App.init();
	//TableManaged.init();
	});
	</script>

</body>
<!-- END BODY -->
</html>