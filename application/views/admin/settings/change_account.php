<?php $this->load->view("admin/templates/header-top.php"); ?>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<?php $this->load->view("admin/templates/header.php"); ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php $this->load->view("admin/templates/sidebar.php"); ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->  
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<?php $this->load->view("admin/templates/nav.php"); ?>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $modulename; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<form action="<?php echo base_url('admin/settings/account'); ?>" method="post" name="form_admin" id="form_sample_2" class="form-horizontal"  enctype="multipart/form-data">
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				You have some form errors. Please check below.
			</div>
			<?php if($this->session->flashdata('success')){	?>
			<div class="alert alert-success hide" style="display: block;">
			<button data-dismiss="alert" class="close"></button>
			<?php echo $this->session->flashdata('success');?>
			</div>
			<?php } ?>
			
			<?php if($error!=''){?>
			<div class="alert alert-error hide" style="display: block;">
			<button data-dismiss="alert" class="close"></button>
			<?php echo $error;?> </div>
			<?php }?>
			<?php if($success!=''){?>
			<div class="alert alert-success hide" style="display: block;">
			<button data-dismiss="alert" class="close"></button>
			<?php echo $success;?> </div>
			<? }?>
			
			
			
			<?php if($veryfy!=2){ ?>
			<div class="alert">Password authentication needed to get account access >>> </div>  
			<div class="control-group">
			<label class="control-label">Password<span class="required">*</span></label>
			<div class="controls">
			<input name="password" id="password" type="password" data-required="1" class="span6 m-wrap"/>
	        </div>
			</div>
			<?php } ?>
			
			<?php if($veryfy!=1){ ?> 
			
			<div class="control-group">
			<label class="control-label">Username<span class="required">*</span></label>
			<div class="controls">
			<input name="username" type="text" required data-required="1"  value="<?=$admin_email->username?>" class="span6 m-wrap"/>
			</div>
			</div>
			<div class="control-group">
			<label class="control-label">Email<span class="required">*</span></label>
			<div class="controls">
			<input name="email" type="text" required data-required="1"  value="<?=$admin_email->email?>" class="span6 m-wrap"/>
			</div>
			</div>
			
			<div class="control-group">
			<label class="control-label">Profile Picture</label>
			<div class="controls">
			<img src="<?php echo base_url("public/image_gallery/user_thumb/$admin_email->image"); ?>" width="264"  /><br>
			<input name="userfile" type="file"  class=""/>
			<input type="hidden" name="userfile_old" value="<?php echo $admin_email->image; ?>"  data-required="1" class="span6 m-wrap"/>	
			</div>
			</div>
			
			<div class="control-group">
			<label class="control-label">Address</label>
			<div class="controls">
			<textarea name="address" type="text" class="span6 m-wrap" ><?=$admin_email->address?></textarea>
			</div>
			</div>
			<div class="control-group">
			<label class="control-label">Phone</label>
			<div class="controls">
			<input name="phone" type="text"  class="span6 m-wrap"  value="<?=$admin_email->phone?>"/>
			</div>
			</div>
			
			<div class="control-group">
			<label class="control-label">Fax</label>
			<div class="controls">
			<input name="fax" type="text"  class="span6 m-wrap"  value="<?=$admin_email->fax?>"/>
			</div>
			</div>
			<?php } ?>
			
            <input type="hidden" name="veryfy" value="<?=$veryfy?>" >
			<div class="form-actions">
				<button type="submit" class="btn green" >Save</button>
				<button type="button" class="btn">Cancel</button>
			</div>
		</form>
		<!-- END FORM-->
	</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->         
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->  
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php $this->load->view("admin/templates/footer.php"); ?>
	<!-- END FOOTER -->

<?php $this->load->view("admin/templates/footer-bottom.php"); ?>