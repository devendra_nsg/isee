<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php print($title);?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="<?=base_url()?>public/admin/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url()?>public/admin/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?=base_url()?>public/admin/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/admin/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/admin/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.ico" />
</head>