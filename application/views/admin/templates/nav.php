<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN STYLE CUSTOMIZER -->
		
		<!-- END BEGIN STYLE CUSTOMIZER -->     
		<h3 class="page-title">
			<?php print($act);?> <?php print($modulename);?>
			<small><?php print($act);?> information</small>
		</h3>
		<ul class="breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="index.html">Home</a> 
				<span class="icon-angle-right"></span>
			</li>
			<li>
				<a href="#"><?php print($modulename);?></a>
				<span class="icon-angle-right"></span>
			</li>
			<li><a href="#"><?php print($act);?></a></li>
		</ul>
	</div>
</div>	