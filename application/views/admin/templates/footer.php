<div class="footer">
		<div class="footer-inner">
			&copy; Copyright <?=date('Y')?> @ fasteducators.com, All Rights Reserved
		</div>
		<div class="footer-tools">
			<span class="go-top">
			<i class="icon-angle-up"></i>
			</span>
		</div>
	</div>