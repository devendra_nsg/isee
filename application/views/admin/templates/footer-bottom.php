		<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   <script src="<?=base_url()?>public/admin/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>public/admin/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="<?=base_url()?>public/admin/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="<?=base_url()?>public/admin/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>public/admin/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<?=base_url()?>public/admin/plugins/excanvas.min.js"></script>
	<script src="<?=base_url()?>public/admin/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="<?=base_url()?>public/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>public/admin/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="<?=base_url()?>public/admin/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>public/admin/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>public/admin/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>public/admin/plugins/jquery-validation/dist/additional-methods.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>public/admin/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>public/admin/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>public/admin/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
	<script type="text/javascript" src="<?=base_url()?>public/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="<?=base_url()?>public/admin/plugins/ckeditor/ckeditor.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<script src="<?=base_url()?>public/admin/scripts/app.js"></script>
	<script src="<?=base_url()?>public/admin/scripts/form-validation.js"></script> 
	<!-- END PAGE LEVEL STYLES -->    
	<script>
		jQuery(document).ready(function() {   
		   // initiate layout and plugins
		   App.init();
		   FormValidation.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>