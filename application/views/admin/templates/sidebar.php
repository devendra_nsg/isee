<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<ul class="page-sidebar-menu">
				<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<?php //echo $modulename;die; ?>
				<li class="start <?php if($modulename=='Dashboard'){ echo "active";} ?>">
					<a href="<?php echo base_url('admin/dashboard'); ?>">
					<i class="icon-home"></i> 
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
				<li class="<?php if($modulename=='Users'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-user"></i> 
					<span class="title">Users</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/users'); ?>">
							<!--<span class="badge badge-roundless badge-important">new (25)</span>-->Manage Users</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Category' || $modulename=='Subcategory'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Category</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/category'); ?>">
							Manage Category</a>
						</li>
						<li >
							<a href="<?php echo base_url('admin/subcategory'); ?>">
							Manage SubCategory</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Schools'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">School</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/schools'); ?>">
							Manage Schools</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Tips'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Tips</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/tips'); ?>">
							Manage Tips</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Powerdownload'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Power download</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/powerdownload'); ?>">
							Manage Power download</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Level' || $modulename=='Level_resource'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Level</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/level'); ?>">
							Manage Level</a>
						</li>
						<li >
							<a href="<?php echo base_url('admin/level_resource'); ?>">
							Manage Level Resource</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Type'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Type</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/type'); ?>">
							Manage Type Resource</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Section'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Section</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/section'); ?>">
							Manage Section</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Exam'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Exam</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/exam'); ?>">
							Manage Exam</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Question' || $modulename=='Learningresource'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Questions</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/questions'); ?>">
							Manage Questions</a>
						</li>
						<li >
							<a href="<?php echo base_url('admin/learningresource'); ?>">
							Manage Learning Resource</a>
						</li>
					</ul>
				</li>
			
				
				<li class="<?php if($modulename=='Cms'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">CMS</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/cms'); ?>">
							Manage CMS</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Block'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Block</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/block'); ?>">
							Manage Block</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Faqs'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">FAQ</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/faqs'); ?>">
							Manage FAQ</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Videos'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Videos</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/videos'); ?>">
							Manage Video</a>
						</li>
					</ul>
				</li>
				
				<li class="<?php if($modulename=='Plan'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Membership</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/plan'); ?>">
							Manage Membership</a>
						</li>
					</ul>
				</li>
				
				
				<li class="<?php if($modulename=='Settings'){ echo "active";} ?>">
					<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">Settings</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li >
							<a href="<?php echo base_url('admin/settings/account'); ?>">
							Manage Account</a>
						</li>
						<li >
							<a href="<?php echo base_url('admin/settings/password'); ?>">
							Manage Password</a>
						</li>
					</ul>
				</li>
				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>