<?php $this->load->view("admin/templates/header-top.php"); ?>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<?php $this->load->view("admin/templates/header.php"); ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php $this->load->view("admin/templates/sidebar.php"); ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->  
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<?php $this->load->view("admin/templates/nav.php"); ?>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $modulename; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<form action="<?php echo base_url('admin/questions/add'); ?>" method="post" name="form_admin" id="form_sample_2" class="form-horizontal">
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				You have some form errors. Please check below.
			</div>
			<div class="alert alert-success hide">
				<button class="close" data-dismiss="alert"></button>
				Your form validation is successful!
			</div>
			
			
			<div class="control-group">
				<label class="control-label">Level<span class="required">*</span></label>
				<div class="controls">
					<select name="level" required class="span6 m-wrap" >
					<option value="">Select Level</option>
					<? foreach($level->result() as $row){ ?>
					<option value="<?=$row->id?>"><?=$row->name?></option>
					<? }?>
					</select>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Section<span class="required">*</span></label>
				<div class="controls">
					<select name="section" required class="span6 m-wrap" >
					<option value="">Select Section</option>
					<? foreach($section->result() as $row){ ?>
					<option value="<?=$row->id?>"><?=$row->name?></option>
					<? }?>
					</select>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Exam<span class="required">*</span></label>
				<div class="controls">
					<select name="exam" required class="span6 m-wrap" >
					<option value="">Select Exam</option>
					<? foreach($exam->result() as $row){ ?>
					<option value="<?=$row->id?>"><?=$row->exam_name?></option>
					<? }?>
					</select>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Question<span class="required">*</span></label>
				<div class="controls">
					<textarea class="span12 ckeditor m-wrap" name="question" required rows="6" data-error-container="#editor2_error"></textarea>
					<div id="editor2_error"></div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Answer<span class="required">*</span></label>
				<div class="controls">
					<textarea class="span12 ckeditor m-wrap" name="answer" required rows="6" data-error-container="#editor2_error"></textarea>
					<div id="editor2_error"></div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Option 1<span class="required">*</span></label>
				<div class="controls">
					<textarea class="span12 ckeditor m-wrap" name="option1" required rows="6" data-error-container="#editor2_error"></textarea>
					<div id="editor2_error"></div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Option 2<span class="required">*</span></label>
				<div class="controls">
					<textarea class="span12 ckeditor m-wrap" name="option2" required rows="6" data-error-container="#editor2_error"></textarea>
					<div id="editor2_error"></div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Option 3<span class="required">*</span></label>
				<div class="controls">
					<textarea class="span12 ckeditor m-wrap" name="option3" required rows="6" data-error-container="#editor2_error"></textarea>
					<div id="editor2_error"></div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Option 4<span class="required">*</span></label>
				<div class="controls">
					<textarea class="span12 ckeditor m-wrap" name="option4" required rows="6" data-error-container="#editor2_error"></textarea>
					<div id="editor2_error"></div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Correct Answer Description</label>
				<div class="controls">
					<textarea class="span12 ckeditor m-wrap" name="description" required rows="6" data-error-container="#editor2_error"></textarea>
					<div id="editor2_error"></div>
				</div>
			</div>
			
			
			
			
			
			
			
			<div class="form-actions">
				<button type="submit" class="btn green" >Save</button>
				<button type="button" class="btn">Cancel</button>
			</div>
		</form>
		<!-- END FORM-->
	</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->         
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->  
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php $this->load->view("admin/templates/footer.php"); ?>
	<!-- END FOOTER -->

<?php $this->load->view("admin/templates/footer-bottom.php"); ?>