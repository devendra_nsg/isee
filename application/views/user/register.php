<div class="wrapper">
    <div class="container">
    	<h1>Get in Touch</h1>
        
		<?php 
		if($this->session->flashdata('error')){?>
		<div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
		<?php } ?>       
     	<?php if($this->session->flashdata('success')){?>
		<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
		<?php } ?>
	    <?php if(isset($error) && !empty($error)){?> <div class="alert alert-error"><?=$error;?></div><?php } ?> 	
		
	<div class="login-signupform">
    	<div class="formLeft">
        	<div class="formimg"><img src="<?php echo base_url("public/images/sitelogo.png")?>" /></div>
        </div>
        <div class="formRight">
        	<div class="formhead">
            	<h3>Unlimited <span>ISEE Testing</span></h3>
            </div>
            <div class="formfields">
            	
                <h3>Secure Signup<span>or Login</span></h3>
                <form method="post" action="<?=base_url('user/register')?>">
                <label>Username</label>
                <input type="text" name="username" id="username" value="<?=set_value('username')?>" />
                <label>Email Id</label>
                <input type="text"  name="email" id="email" value="<?=set_value('email')?>"/>
                <label>Password</label>
                <input class="smallinput" type="password" name="password" id="password"  />
                 <label>Confirm Password</label>
                <input class="smallinput" type="password"  name="confirmpassword" id="confirmpassword"/>
                <div class="clear"></div>
                 <span class="agreemnt"> <input type="checkbox" name="agreement" id="agreement"  />I have read and agree to <a class="bluelink" href="<?php echo base_url("page/terms")?>">terms and services</a></span>
                <div class="clear"></div>
                <input type="submit" value="Register" />
                </form>
            </div>
        </div>
    <div class="clear"></div>	
    </div>
	
    </div>