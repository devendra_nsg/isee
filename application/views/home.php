 <div class="wrapper">
    <div class="container categoryChoose">
    	
        <h2 class="caps textCenter">Choose a Category</h2>
        <div class="block three">
        	<div class="tests textCenter">
            	<a href="sectiontest"> <span>ISEE</span> Section Tests</a>
            </div>
            
            
        </div>
        <div class="block three">
        	<div class="tests textCenter">
            	<a href="fulltest"> <span>Full</span> ISEE Tests</a>
            </div>
            
            
        </div>
        <div class="block three">
        	<div class="tests textCenter">
            	<a href="learnresource/type"> <span>ISEE</span> <span>Learning</span> Resources </a>
            </div>
            
            
        </div>
    	
    </div>
    <div class="container categories">
    	<?php echo $query['masterIsee']->description; ?>
        <div class="block three">
        	<img src="<?php echo base_url("public/images/cat1.jpg")?>" alt="" />
        </div>
        <div class="block three">
        	<img src="<?php echo base_url("public/images/cat2.jpg")?>" alt="" />
        </div>
        <div class="block three">
        	<img src="<?php echo base_url("public/images/cat3.jpg")?>" alt="" />
        </div>
    </div>
    <div class="videosWrap">
    	<div class="container">
        	<div class="block three">
			<?php foreach($query['videos']->result() as $row){ ?>
            	<div class="video">
                	<!--<img src="<?php echo base_url("public/image_gallery/video_thumb/$row->video_img"); ?>" alt="" />-->
					<iframe width="370" height="220" src="<?php echo str_replace('http://youtu.be/','//www.youtube.com/embed/',$row->video_link);?>" frameborder="0" allowfullscreen></iframe>
                </div>
			<?php } ?>	
            </div>
            <div class="block twoThird">
            	<ul class="features">
				<?php echo $query['instruction']->description; ?>
                </ul>
            </div> 
        </div>
    </div>
    <?php $this->load->view('/templates/fb-fanbox.php'); ?>