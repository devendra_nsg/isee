<?php

class Level_resource_Model extends CI_Model{
	
	function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	function get_all_Level_resource(){
		
		return $this->db->get('level_resource');
		
	}
	
	function insert_level_resource($data){
	
		$this->db->insert('level_resource',$data);
		return $this->db->insert_id();
	}
	
	function get_level_resource($id){
	
		return $this->db->get_where('level_resource',array('id'=>$id))->row();
	}
	
	function get_active_level_resource(){
	
		return $this->db->get_where('level_resource',array('status'=>1))->result();
	}
	
	function update_level_resource($data){
	
		$id = $data['id'];
		$this->db->where('id',$id);
		$this->db->update('level_resource',$data);
	}
	
	
	
	
	
}


