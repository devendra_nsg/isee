<?php
class Question_Model extends CI_Model{
	
	public function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	public function get_all_question(){
		
		$this->db->select('question.*, section.name as secname,level.name as levelname,exam.exam_name as examname');
		$this->db->from('question');
		$this->db->join('section','section.id=question.section');
		$this->db->join('level','level.id=question.level');
		$this->db->join('exam','exam.id=question.exam');
		return $this->db->get();
	}
	
	public function _get_subcate($cateId){		
		return $this->db->get_where('question',array('cate_id'=>$cateId,'status'=>1))->result();		
	}
	
	public function insert_question($data){
	
	// echo "<pre>";print_r($data); exit;
		$this->level	= $data['level'];
		$this->exam 	= $data['exam'];
		$this->section = $data['section'];
		$this->question = $data['question'];
		$this->description =$data['description'];
		$this->option1 	= $data['option1'];
		$this->option2 	= $data['option2'];
		$this->option3 	= $data['option3'];
		$this->option4 	= $data['option4'];
		$this->answer 	= $data['answer'];

		return $this->db->insert('question',$this);
	
	}
	
	public function get_question($id){
		return $this->db->get_where('question',array('id'=>$id))->row();
	}
	
	public function update_question($data){

		$this->level	= $data['level'];
		$this->exam 	= $data['exam'];
		$this->section = $data['section'];
		$this->question = $data['question'];
		$this->description =$data['description'];
		$this->option1 	= $data['option1'];
		$this->option2 	= $data['option2'];
		$this->option3 	= $data['option3'];
		$this->option4 	= $data['option4'];
		$this->answer 	= $data['answer'];
		
		$this->db->where('id',$_POST['id']);
		return $this->db->update('question',$this);
	}	
		

		
		
	public function insert_question_csv($data){
		
		$this->level	= $data['level'];
		$this->exam 	= $data['exam'];
		$this->section = $data['section'];
		$this->question = $data['question'];
		$this->description =$data['description'];
		$this->option1 	= $data['option1'];
		$this->option2 	= $data['option2'];
		$this->option3 	= $data['option3'];
		$this->option4 	= $data['option4'];
		$this->answer 	= $data['answer'];
		$this->image 	= $data['image'];
		$this->video 	= $data['video'];
		
		return $this->db->insert('question',$this);
	
	}
	
	
}
