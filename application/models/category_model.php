<?php

class Category_Model extends CI_Model{
	
	function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	function get_all_category(){
		
		return $this->db->get('categories');
		
	}
	
	function insert_category($data){
	
		$this->db->insert('categories',$data);
		return $this->db->insert_id();
	}
	
	function get_category($id){
	
		return $this->db->get_where('categories',array('id'=>$id))->row();
	}
	
	function get_active_category(){
	
		return $this->db->get_where('categories',array('status'=>1))->result();
	}
	
	function update_category($data){
	
		$id = $data['id'];
		$this->db->where('id',$id);
		$this->db->update('categories',$data);
	}
	
	
	
	
	
}


