<?php
class Country_Model extends CI_Model{
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
				$this->load->database();
    }
	
	
	
	function get_country($id)
	
	{
	
	$this->db->select('countryname');
	$this->db->from('gfa_country');
	$this->db->where('id',$id);
	$query=$this->db->get();
	return $query->row_array();
	
	}
	
	
	
	
	function get_state($id)
	
	{
	
	$this->db->select('countryname');
	$this->db->from('gfa_country');
	$this->db->where('id',$id);
	$query=$this->db->get();
	return $query->row_array();
	
	}
	
	
	
	
	function get_city($id)
	
	{
	
	$this->db->select('countryname');
	$this->db->from('gfa_country');
	$this->db->where('id',$id);
	$query=$this->db->get();
	return $query->row_array();
	
	}
	
	
	function get_allcountry()
	
	{
	
	$this->db->select('*');
	$this->db->from('gfa_country');
	$this->db->where('parentid',0);
	$query=$this->db->get();
	return $query->result_array();
	
	}
	
	}
	?>