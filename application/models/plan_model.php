<?php
class plan_model extends CI_Model{

	 public function __construct()
		{
			// Call the Model constructor
			parent::__construct();
			$this->load->database();
		}
	

		public function get_all_plans()
		{
			$this->db->select('*');
			$this->db->from('plan');
			$query=$this->db->get();
			
			return $query;
		}

		public function get_plan($id)
		
		{
			$this->db->select("*");
			$this -> db -> from('plan');
			$this -> db -> where('id',$id);
			
			$query = $this->db->get();
			 if($query->num_rows()>0){
			      return $query->row();	
			  }else{ 
				  return 'Not found'; 
			  }
		}


		public function update_plan($data)
		{   
			$this->db->where('id',$data['id']);
		    return $this->db->update('plan',$data);
		}
		








 


}
