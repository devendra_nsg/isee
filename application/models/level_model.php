<?php

class Level_Model extends CI_Model{
	
	function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	function get_all_level(){
		
		return $this->db->get('level');
		
	}
	
	function insert_level($data){
	
		$this->db->insert('level',$data);
		return $this->db->insert_id();
	}
	
	function get_level($id){
	
		return $this->db->get_where('level',array('id'=>$id))->row();
	}
	
	function get_active_level(){
	
		return $this->db->get_where('level',array('status'=>1))->result();
	}
	
	function update_level($data){
	
		$id = $data['id'];
		$this->db->where('id',$id);
		$this->db->update('level',$data);
	}
	
	
	
	
	
}


