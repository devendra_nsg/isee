<?php

class Faqs_Model extends CI_Model{
	
	 public function __construct()
    {
			  // Call the Model constructor
        parent::__construct();
		
				$this->load->database();
    }
		
	public function get_all_faqs(){
		
			$this -> db -> select('*');
			$this -> db -> from('faq');
			$this -> db -> order_by('id','desc');
			
			$query = $this->db->get();
			return $query;			
		}
		
	public function get_faq($id){
		
			$data = $this->db->get_where('faq',array('id'=>$id));
			return $data->row();
		}
		
	public function add_faq($data){
		    
			$this->question = $data['question'];
			$this->answer = $data['answer'];
			$this->status = $data['status'];
			$this->db->insert('faq',$this);	
			return $this->db->insert_id();			
		}
		
	public function update_faq($data){
		
			$id = $data['id'];
			$this->question = $data['question'];
			$this->answer = $data['answer'];
			$this->status = $data['status'];
			
			$this->db->where('id',$id);
			$this->db->update('faq',$this);
	 }
	 
	 public function delete($id){
	 
		 $this->db->where('id', $id);
		 $this->db->delete('faq');
	 }
			
			
		
}