<?php

class  Powerdownload_Model extends CI_Model{
	
	function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	function get_all_powerdownload(){
		
		return $this->db->get('powerdownload');
		
	}
	
	function insert_powerdownload($data){
	
		$this->db->insert('powerdownload',$data);
		return $this->db->insert_id();
	}
	
	function get_powerdownload($id){
	
		return $this->db->get_where('powerdownload',array('id'=>$id))->row();
	}
	
	function get_active_powerdownload(){
	
		return $this->db->get_where('powerdownload',array('status'=>1));
	}
	
	function update_powerdownload($data){
	
		$id = $data['id'];
		$this->db->where('id',$id);
		$this->db->update('powerdownload',$data);
	}
	
	
	
	
	
}


