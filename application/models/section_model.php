<?php

class Section_Model extends CI_Model{
	
	function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	function get_all_section(){
		
		return $this->db->get('section');
		
	}
	
	function insert_section($data){
	
		$this->db->insert('section',$data);
		return $this->db->insert_id();
	}
	
	function get_section($id){
	
		return $this->db->get_where('section',array('id'=>$id))->row();
	}
	
	function get_active_section(){
	
		return $this->db->get_where('section',array('status'=>1))->result();
	}
	
	function update_section($data){
	
		$id = $data['id'];
		$this->db->where('id',$id);
		$this->db->update('section',$data);
	}
	
	
	
	
	
}


