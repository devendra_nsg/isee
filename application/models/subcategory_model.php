<?php

class Subcategory_Model extends CI_Model{
	
	public function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	public function get_all_subcategory(){
		
		$this->db->select('sub_category.*,categories.name as catname');
		$this->db->from('sub_category');
		$this->db->join('categories','categories.id=sub_category.cate_id');
		return $this->db->get();
		
	}
	
	public function _get_subcate($cateId){		
		return $this->db->get_where('sub_category',array('cate_id'=>$cateId,'status'=>1))->result();		
	}
	
	public function insert_subcategory($data){
		$this->name = $data['name'];
		$this->cate_id = $data['cate_id'];
		$this->status = 1;
		$this->long_description = $data['long_description'];
		
		return $this->db->insert('sub_category',$this);
	
	}
	
	public function get_subcategory($id){
		return $this->db->get_where('sub_category',array('id'=>$id))->row();
	}
	
	public function update_subcategory(){
		$this->name = $_POST['name'];
		$this->cate_id = $_POST['cate_id'];
		$this->status = $_POST['status'];
		$this->long_description = $_POST['long_description'];
		
		$this->db->where('id',$_POST['id']);
		return $this->db->update('sub_category',$this);
	}	
		
	function getSubcategoryByTopicId($catId) {
		$this->db->where("cate_id", $catId);
		return $this->db->get("sub_category")->result_array();
	}
	
	
	
}
