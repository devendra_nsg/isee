<?php

class Block_Model extends CI_Model{
	
	 public function __construct()
    {
			  // Call the Model constructor
        parent::__construct();
				$this->load->database();
    }
		
	public function get_all_block(){
		
			$this -> db -> select('*');
			$this -> db -> from('block');
		 	$this -> db -> order_by('id','desc');
		
		 	$query = $this->db->get();
            return $query;		
		}
		
	public function get_block($id){
		
			$data = $this->db->get_where('block',array('id'=>$id))->row();
			return $data;
		}
		
	public function update_block($data){
			
			$id = $data['id'];
			$this->db->where('id',$id);
			$this->db->update('block',$data);			
		}
		
	public function add_block($data){
		
			$this->db->insert('block',$data);
			return $this->db->insert_id();				
		}
				
	public function get_aboutus(){
		
			$this -> db -> select('*');
			$this -> db -> from('block');
			$this -> db -> where('id','1');
			
			$query = $this->db->get();
			return $query->row();	
		
		}
				
				
				
	public function get_howitworks()
		{
			$this -> db -> select('*');
			$this -> db -> from('block');
			$this -> db -> where('id','2');
			
			$query = $this->db->get();
			return $query->row();	
		
		}
			
				
					
	public	function get_privacypolicy()
		{
			$this -> db -> select('*');
			$this -> db -> from('block');
			$this -> db -> where('id','3');
			
			$query = $this->db->get();
			return $query->row();	
		
		}	
		
	public	function get_termofuse()
		{
			$this -> db -> select('*');
			$this -> db -> from('block');
			$this -> db -> where('id','4');
			
			$query = $this->db->get();
			return $query->row();	
		
		}	
			
		
	public	function get_membership_agreement()
		{
			$this -> db -> select('*');
			$this -> db -> from('block');
			$this -> db -> where('id','5');
			
			$query = $this->db->get();
			return $query->row();
		}	

		
}