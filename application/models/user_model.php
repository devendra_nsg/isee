<?php

class User_Model extends CI_Model{
	
	 function __construct()
    {
        // Call the Model constructor
           parent::__construct();
		   $this->load->database();
    }
	
	
	public function insert_user($data){
	
		$data['register_date'] = time();
		$this->db->insert('users',$data);
		return $this->db->insert_id();	
	}
	
	public function update_user($data){
        
		$id = $data['id'];
		$this->db->where('id', $id);
		return $this->db->update('users', $data); 					
	}
	
		
	public function delete_user($id){
		$this->db->where('id', $id);
		$this->db->delete('users');
	}
	
	////function to get all membership////
	function getAllMembership() {
		return $this->db->get("plan")->result_array();
	}
	
	
	
	function check_email($email){
		$this->db->select("email");
		$this -> db -> from('users');
		
		$query = $this->db->get();
		if($query->num_rows()>0){
				return 'exists';
		}else{
				return 'available';
		}
	}
	
	
	public function get_all_users(){
			
		 $this -> db -> select('users.*,plan.duration');
		 $this -> db -> from('users');
		 $this -> db -> join('plan','plan.id=users.plan_id');
		 $this -> db -> order_by('id','desc');
		
	     return	 $query = $this->db->get();
      
	}
	
	public function get_user($id){
	
		$this -> db -> select("*");
		$this -> db -> from('users');
		$this -> db -> where('id',$id);
		
		$query = $this->db->get();
		if($query->num_rows()>0) { 
		  return $query->row();	
		}else{
		  return 'Not found';
		}
	}
	
	
}