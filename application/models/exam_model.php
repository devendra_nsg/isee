<?php

class Exam_Model extends CI_Model{
	
	public function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	public function get_all_exam(){
		
		$this->db->select('exam.*');
		$this->db->from('exam');
		//$this->db->join('sub_category','exam.cat_id=sub_category.id');
		return $this->db->get();
		
	}
	
	
	public function insert_exam($data){
	
	  
		$this->exam_name = $data['exam_name'];
		
		$this->status = 1;
		$this->description = $data['description'];
		$this->time_duration = $data['time_duration'];
		
		$this->percentage = $data['percentage'];
		
		return $this->db->insert('exam',$this);
	
	}
	
	public function get_exam($id){
		return $this->db->get_where('exam',array('id'=>$id))->row();
	}
	
	public function update_exam(){
		
		
		//echo "<pre>";print_r($_POST);exit;
		$this->exam_name = $_POST['exam_name'];	
		$this->description = $_POST['description'];
		$this->time_duration = $_POST['time_duration'];
		
		$this->percentage = $_POST['percentage'];
		$this->status = $_POST['status'];
		
		$this->db->where('id',$_POST['id']);
		return $this->db->update('exam',$this);
	}	
		

	
	
	
}
