<?php

class Type_Model extends CI_Model{
	
	function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	function get_all_type(){
		
		return $this->db->get('type');
		
	}
	
	function insert_type($data){
	
		$this->db->insert('type',$data);
		return $this->db->insert_id();
	}
	
	function get_type($id){
	
		return $this->db->get_where('type',array('id'=>$id))->row();
	}
	
	function get_active_type(){
	
		return $this->db->get_where('type',array('status'=>1))->result();
	}
	
	function update_type($data){
	
		$id = $data['id'];
		$this->db->where('id',$id);
		$this->db->update('type',$data);
	}
	
	
	
	
	
}


