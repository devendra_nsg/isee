<?php
class Learningresource_Model extends CI_Model{
	
	public function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	public function get_all_question(){
		
		$this->db->select('learningresource.*, type.name as typename, categories.name as catname, sub_category.name as subcatname,level.name as levelname');
		$this->db->from('learningresource');
		$this->db->join('categories','categories.id=learningresource.topic');
		$this->db->join('sub_category','sub_category.id=learningresource.subject');
		$this->db->join('level','level.id=learningresource.level');
		$this->db->join('type','type.id=learningresource.type');
		return $this->db->get();
	}
	
	public function _get_subcate($cateId){		
		return $this->db->get_where('learningresource',array('cate_id'=>$cateId,'status'=>1))->result();		
	}
	
	public function insert_question($data){

	    $this->type 	= $data['type'];
		$this->level	= $data['level'];
		$this->topic = $data['category'];
		$this->subject = $data['subcategory'];
		$this->question = $data['question'];
		$this->answer 	= $data['answer'];
		$this->video 	= $data['video'];
		$this->title 	= $data['title'];
		return $this->db->insert('learningresource',$this);

	}
	
	public function get_question($id){
		return $this->db->get_where('learningresource',array('id'=>$id))->row();
	}
	
	public function update_question($data){
		//print_r($data);die;
		$this->type 	= $data['type'];
		$this->level	= $data['level'];
		$this->topic = $data['category'];
		$this->subject = $data['subcategory'];
		$this->question = $data['question'];
		$this->answer 	= $data['answer'];
		$this->video 	= $data['video'];
		
		$this->db->where('id',$data['id']);
		return $this->db->update('learningresource',$this);
	}	
		
	/////get topics ////
	public function get_learning_resource_topic() {
		return $this->db->get("categories")->result_array();
	}
	
	public function get_learning_resource_subject($id) {
		$this->db->where("cate_id", $id);
		return $this->db->get("sub_category")->result_array();
	}
	
	public function get_learning_resource_question($type, $level, $topic, $subject) {
		$this->db->where("type", $type);
		$this->db->where("level", $level);
		$this->db->where("topic", $topic);
		$this->db->where("subject", $subject);
		return $this->db->get("learningresource")->result_array();
	}
	
	public function getTypeById($id) {
		$this->db->where("id", $id);
		$type = $this->db->get("type")->row_array();
		return $type;
	}
	
	public function getLevelById($id) {
		$this->db->where("id", $id);
		$level = $this->db->get("level_resource")->row_array();
		return $level;
	}
	
	public function getTopicById($id) {
		$this->db->where("id", $id);
		$topic = $this->db->get("categories")->row_array();
		return $topic;
	}
	public function get_learning_resource_videos() {
		return $this->db->get("videos")->result_array();
	}
	
	
	public function insert_question_csv($data){
		//id,type,level,topic,subject,question,answer,video,file
		$this->type 	= $data['type'];
		$this->level	= $data['level'];
		$this->topic = $data['topic'];
		$this->subject = $data['subject'];
		$this->question = $data['question'];
		$this->answer 	= $data['answer'];
		$this->video 	= $data['video'];
		return $this->db->insert('learningresource',$this);
	}
	
}
