<?php
class Dashboard_model extends CI_Model
{
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	function get_all_users($range=''){
		$this->db->select('user_id');
		$this->db->from('users');
		$query = $this->db->get();
   	return $query;
	}
	
	function get_active_users(){
		$this->db->select('user_id');
		$this->db->from('users');
		$this->db->where('status',1);
				
		$query = $this->db->get();
    return $query;		 
	}
	
	function get_all_tasks(){
		$this->db->select('task_id');
		$this->db->from('tasks');
						
		$query = $this->db->get();
    return $query;		 
	}
	
	function get_completed_tasks(){
		$this->db->select('task_id');
		$this->db->from('tasks');
		$this->db->where('status',2);
				
		$query = $this->db->get();
    return $query;		 
	}
	
		function get_total_earnigs(){
		$this->db->select('sum(balance) as earning');
		$this->db->from('users');
		$this->db->where('status',1);
		$this->db->where('user_type','user');
				
		$query = $this->db->get();
    return $query->row()->earning;		 
	}
	
	function recent_tasks(){				
		$this->db->select('*');
		$this->db->from('tasks');
		$this->db->where('status',0);
		$this->db->order_by('task_id','desc');
		$this->db->limit(6);
		
		$query = $this->db->get();
    return $query;
	}
	
	function recent_comments(){				
		$this->db->select('*');
		$this->db->from('comments');
		$this->db->where('status',1);
		$this->db->order_by('comment_id','desc');
		$this->db->limit(6);
		
		$query = $this->db->get();
		return $query;
	}
	
	////count all users/////
	function countUsers() {
		return $this->db->count_all('users');
	}
}

?>
		