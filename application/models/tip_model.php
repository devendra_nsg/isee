<?php

class  Tip_Model extends CI_Model{
	
	function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	function get_all_tip(){
		
		return $this->db->get('tip');
		
	}
	
	function insert_tip($data){
	
		$this->db->insert('tip',$data);
		return $this->db->insert_id();
	}
	
	function get_tip($id){
	
		return $this->db->get_where('tip',array('id'=>$id))->row();
	}
	
	function get_active_tip(){
	
		return $this->db->get_where('tip',array('status'=>1));
	}
	
	function update_tip($data){
	
		$id = $data['id'];
		$this->db->where('id',$id);
		$this->db->update('tip',$data);
	}
	
	
	
	
	
}


