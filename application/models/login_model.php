<?php
class Login_model extends CI_Model
{
	
	public function admin_login($username, $password){
	
	   $this -> db -> select('id, username, password');
	   $this -> db -> from('admin');
	   $this -> db -> where('username', $username);
	   $this -> db -> where('password', $password);
	 
	   $query = $this -> db -> get();
	 
	   if($query -> num_rows() == 1)
	   {
	     return $query->row();
	   }
	   else
	   {
	     return false;
	   }
	}
	
	
	public function get_email()
	 {
	    $this->db->select("*");
	    $this -> db -> from('admin');
	    return $this->db->get()->row();
	 }
	
	 
	public function check_admin_email($email){
					
		$this -> db -> select('id,email,password');
		$this -> db -> from('admin');
		$this -> db -> where('email', $email);
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() == 1)
		{
		   return $query->row();
		}
		else
		{
		   return false;
		}
		
	  }
	  
	  
	  
	  //FRONT END ******* login function for users  ************
	  
	 public function user_login($email, $password){
	
	   $this -> db -> select('id, username,email,password');
	   $this -> db -> from('users');
	   $this -> db -> where('email', $email);
	   $this -> db -> where('password', $password);
	 
	   $query = $this -> db -> get();
	 
	   if($query -> num_rows() == 1)
	   {
	     return $query->row();
	   }
	   else
	   {
	     return false;
	   }
	}
	
	public function user_login_fb($email){
	
	   $this -> db -> select('id, username,email,password');
	   $this -> db -> from('users');
	   $this -> db -> where('email', $email);
	 
	   $query = $this -> db -> get();
	 
	   if($query -> num_rows() == 1)
	   {
	     return $query->row();
	   }
	   else
	   {
	     return false;
	   }
	}
	
	 
	public function check_email($email){
		$this->db->select("email");
		$this -> db -> from('users');
		$this -> db -> where('email',$email);
		
		
		$query = $this->db->get();
		if($query->num_rows()>0){
		return 'exists';
		}else{
		return 'available';
		}
	}
		
	
	public function get_email_user()
	{
	  $this->db->select("*");
	  $this -> db -> from('users');
	  return $this->db->get()->row_array();
	}
	  
	  
	public function insert_user($data)
	{   //print_r($data);die;
		$this->username = $data['username'];
		$this->email = $data['email'];
		$this->password = $data['password'];
		$this->register_date = time();
		$this->db->insert('users',$this);
		return $this->db->insert_id();
	}
	
	
}