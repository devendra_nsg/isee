<?php 
class Videos_Model extends CI_Model{

	 public function  __construct(){
		  parent::__construct();
		  $this->load->database();
	 }

	  public function get_all_videos(){
		 $this->db->select("*");
		 $this->db->from("videos");
		 $this -> db -> order_by('id','desc');
		 
		 $query=$this->db->get();
		 return $query;
	  }
	 
	 
	  public  function get_video($id){
			$data = $this->db->get_where('videos',array('id'=>$id));
			return $data->row();
		}
	 
	 
	  public function add_video($data){ 
	        $this->title = $data['title'];
			$this->video_link = $data['video_link'];
			$this->disc = $data['disc'];
			$this->video_img = $data['video_img'];
			$this->status=$data['status'];
			$this->dates = time();
			
			return $this->db->insert('videos',$this);				
		}
		
	   public function update_video($data){
			$this->title = $data['title'];
			$this->video_link = $data['video_link'];
			$this->disc = $data['disc'];
			$this->video_img = $data['video_img'];
			$this->status=$data['status'];
			
			$this->db->where('id',$data['id']);
			return $this->db->update('videos',$this);		
		}
	 
	  
		public  function delete($id){
			$this->db->where('id', $id);
			return $this->db->delete('videos');
		}
		
		
}
