<?php

class  School_Model extends CI_Model{
	
	function __construct(){
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	function get_all_school(){
		
		return $this->db->get('school');
		
	}
	
	function insert_school($data){
	
		$this->db->insert('school',$data);
		return $this->db->insert_id();
	}
	
	function get_school($id){
	
		return $this->db->get_where('school',array('id'=>$id))->row();
	}
	
	function get_active_school(){
	
		return $this->db->get_where('school',array('status'=>1))->result();
	}
	
	function update_school($data){
	
		$id = $data['id'];
		$this->db->where('id',$id);
		$this->db->update('school',$data);
	}
	
	
	
	
	
}


